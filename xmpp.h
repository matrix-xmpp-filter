#ifndef XMPP_H
#define XMPP_H

#include<qxmpp/QXmppClient.h>
#include<qxmpp/QXmppMessage.h>
#include <QObject>
#include "filter.h"
#include "matrix.h"

class Forwarder;

/**
 * @brief The Xmpp class represents an xmpp user that owns one or more matrix connections
 */
class Xmpp : public QObject
{
    Q_OBJECT
public:
    explicit Xmpp(QObject *parent = nullptr);
    QList<Filter*> filterList;

    void setXmppRecipient(const QString&jid);
    /**
     * @brief clientMode initialize client mode
     */
    void clientMode();
    void connectToServer(const QString&jid, const QString&pass);
    /**
     * @brief roomJids key is matrix room id, value is jid
     */
    QMap<QString,QString> roomJids;
    Room * getRoomWithJid(const QString &jid);
    QString getJidWithRoom(Room * room);
    Forwarder *forwarder;
    QString account;
    void disconnectMatrix();
    void disconnectXmpp();

signals:

public slots:
    void matrixMessage(Matrix *m, Room* room, const QString& author, const QString&msg);
    void addRoom(Room * room);
    void messageFromXmppClientMode(const QXmppMessage& message);
    void xmppError(QXmppClient::Error error);
    void xmppConnected();
    QString command(const QString &command);
    QString filterCommand(const QString &command);
    void xmppDisconnected();
    void connectMatrix();
    /**
     * @brief matrixMessageSent notifiex xmpp client that message is sent to matrix
     * @param txnId transaction id from libQuotient
     * @param eventId eventId from libQuotient
     */
    void matrixMessageSentClientMode(QString txnId, QString eventId);

private:
    QXmppClient *xc;
    QList<Matrix*> matrixList;
    QString xmppRecipient;
    QTimer syncTimer;
    Filter* getFilter(const QString &name,bool createIfNotFound=true);
    Matrix *replyMatrix;
    Room *replyRoom;
    QString previousAccount;
    QString previousRoom;
    QString previousAuthor;
    QString listRooms(const QString &args);
    QString getXmppRoom();
    QString setXmppRoom(const QString &value);
    Matrix* getMatrix(const QString &name);
    void xmppReconnect();
    /**
     * @brief jidRooms key is jid, value is pointer to libQuotient Room
     */
    QMap<QString, Room *> jidRooms;
    QString nodePrep(const Room * room);
    bool matrixConnected;
};

#endif // XMPP_H
