#include "filter.h"
#include "logging.h"
#include "settings.h"
#include <QDateTime>
#include <QTime>
#include <QDebug>

#define NO_WEEKDAY_MATCH { qCDebug(FILTER) << name << "weekday doesn't match"; return 0; } break
const QString Filter::ACTIVE(QStringLiteral("active"));
const QString Filter::AUTHORRE(QStringLiteral("authorre"));
const QString Filter::ROOMRE(QStringLiteral("roomre"));
const QString Filter::MESSAGERE(QStringLiteral("messagere"));
const QString Filter::ACCOUNTRE(QStringLiteral("accountre"));
#define ACCOUNT QStringLiteral("account")
#define ROOM QStringLiteral("room")
#define AUTHOR QStringLiteral("author")
#define MESSAGE QStringLiteral("message")
#define MO QStringLiteral("mo")
#define TU QStringLiteral("tu")
#define WE QStringLiteral("we")
#define TH QStringLiteral("th")
#define FR QStringLiteral("fr")
#define SA QStringLiteral("sa")
#define SU QStringLiteral("su")
#define START QStringLiteral("start")
#define END QStringLiteral("end")
#define SCORE QStringLiteral("score")


Filter::Filter(QObject *parent) : QObject(parent)
{
    active=mo=tu=we=th=fr=sa=su=true;
    start.setHMS(0,0,0);
    end.setHMS(0,0,0);
    score=0;
}

Filter::Filter(QVariant account_name, QVariant filter_name, QVariant act, QVariant Mo, QVariant Tu, QVariant We, QVariant Th, QVariant Fr, QVariant Sa, QVariant Su, QVariant Start, QVariant End, QVariant AccountRe, QVariant RoomRe, QVariant AuthorRe, QVariant MessageRe,QVariant Score)
{
   //QVariantMap map=Settings::getInstance().getFilter(xmppAccount,QStringLiteral(""),{DISPLAY_NAME,ACTIVE,MO,TU,WE,TH,FR,SA,SU,START,END,SCORE,ACCOUNT,ROOM,AUTHOR,MESSAGE});
   if(!filter_name.isNull())
       name=filter_name.toString();
   if(!account_name.isNull())
       xmppAccount=account_name.toString();
   active=qvariantBoolDefaultTrue(act);
   mo=qvariantBoolDefaultTrue(Mo);
   tu=qvariantBoolDefaultTrue(Tu);
   we=qvariantBoolDefaultTrue(We);
   th=qvariantBoolDefaultTrue(Th);
   fr=qvariantBoolDefaultTrue(Fr);
   sa=qvariantBoolDefaultTrue(Sa);
   su=qvariantBoolDefaultTrue(Su);
   if(Start.isNull()) {
       start.setHMS(0,0,0);
   }else
        start=Start.toTime();
   if(End.isNull())
       end.setHMS(0,0,0);
   else
       end=End.toTime();
   accountRe.setPattern(AccountRe.toString());
   roomRe.setPattern(RoomRe.toString());
   authorRe.setPattern(AuthorRe.toString());
   messageRe.setPattern(MessageRe.toString());
   score=Score.toLongLong();
}

qint64 Filter::getScore(const QString &account, const QString &room, const QString &author, const QString &message)
{
    if(!active || score==0) {
        qCDebug(FILTER) << "filter " << name << "active =" << active << "score =" << score;
        return 0;
    }

    QDate nowd=QDate::currentDate();
    switch(nowd.dayOfWeek()) {
    case 0:
        qCDebug(FILTER) << "filter" << name << "bad date" << nowd << "Treating filter as active today.";
        break;
    case 1:
        if(!mo) NO_WEEKDAY_MATCH;
    case 2:
        if(!tu) NO_WEEKDAY_MATCH;
    case 3:
        if(!we) NO_WEEKDAY_MATCH;
    case 4:
        if(!th) NO_WEEKDAY_MATCH;
    case 5:
        if(!fr) NO_WEEKDAY_MATCH;
    case 6:
        if(!sa) NO_WEEKDAY_MATCH;
    case 7:
        if(!su) NO_WEEKDAY_MATCH;
    }

    if(start!=end) {
        QTime nowt=QTime::currentTime();
        qCDebug(FILTER) << "filter" << name << "start =" << start << "end =" << end << "now =" << nowt;
        if(
                (start < end && (nowt < start || nowt >= end))
                || (end < start && nowt >= end && nowt < start)
           ) {
            qCDebug(FILTER) << "filter" << name << "time doesn't match";
            return 0;
        }else{
            qCDebug(FILTER) << "filter" << name << "time matches";
        }
    }

    if(accountRe.match(account).hasMatch()
            && roomRe.match(room).hasMatch()
            && authorRe.match(author).hasMatch()
            && messageRe.match(message).hasMatch()) {
        qCDebug(FILTER) << name << "gives score" << score;
        return score;
    }
    qCDebug(FILTER) << name << "no regex match";
    return 0;
}

void Filter::setXmppAccount(const QString &xmppAccount)
{
    this->xmppAccount=xmppAccount;
}

QString Filter::getName()
{
    return name;
}

void Filter::setName(const QString &name)
{
    this->name=name;
}

QString Filter::toString()
{
    bool showTimes=(start.hour()!=0 || start.minute()!=0 || end.hour()!=0 || end.minute()!=0 );
    bool showWeekdays=!(mo && tu && we && th && fr && sa && su);
    return name
            +(active?QStringLiteral():QStringLiteral(" not active"))
            +QStringLiteral(" score=")+QString::number(score)
            +(showTimes?" "+start.toString()+"-"+end.toString():QStringLiteral())
            +(showWeekdays?(
                               (mo?QStringLiteral(" mo"):QStringLiteral())
                               +(tu?QStringLiteral(" tu"):QStringLiteral())
                               +(we?QStringLiteral(" we"):QStringLiteral())
                               +(th?QStringLiteral(" th"):QStringLiteral())
                               +(fr?QStringLiteral(" fr"):QStringLiteral())
                               +(sa?QStringLiteral(" sa"):QStringLiteral())
                               +(su?QStringLiteral(" su"):QStringLiteral())
                               ):QStringLiteral())
            +(accountRe.pattern().isNull()?QStringLiteral():(QStringLiteral(" accout=\"")+accountRe.pattern()+"\""))
            +(roomRe.pattern().isNull()?QStringLiteral():(QStringLiteral(" room=\"")+roomRe.pattern()+"\""))
            +(authorRe.pattern().isNull()?QStringLiteral():(QStringLiteral(" author=\"")+authorRe.pattern()+"\""))
            +(messageRe.pattern().isNull()?QStringLiteral():(QStringLiteral(" message=\"")+messageRe.pattern()+"\""))
            ;
}

bool Filter::setAccountRe(const QString &re)
{
    accountRe.setPattern(re);
    return Settings::getInstance().setFilter(xmppAccount,name,ACCOUNTRE,re);
}

bool Filter::setRoomRe(const QString &re)
{
    roomRe.setPattern(re);
    return Settings::getInstance().setFilter(xmppAccount,name,ROOMRE,re);
}

bool Filter::setAuthorRe(const QString &re)
{
    authorRe.setPattern(re);
    return Settings::getInstance().setFilter(xmppAccount,name,AUTHORRE,re);
}

bool Filter::setMessageRe(const QString &re)
{
    messageRe.setPattern(re);
    return Settings::getInstance().setFilter(xmppAccount,name,MESSAGERE,re);
}

bool Filter::configBoolDefaultTrue(const QString &key, const QVariantMap &map) {
    QVariant v=map[key];
    if(v.isNull())
        return true;
    return v.toBool();
}

bool Filter::qvariantBoolDefaultTrue(const QVariant value) {
    if(value.isNull())
        return true;
    return value.toBool();
}

bool Filter::read(const QVariantMap map)
{
   //QVariantMap map=Settings::getInstance().getFilter(xmppAccount,QStringLiteral(""),{DISPLAY_NAME,ACTIVE,MO,TU,WE,TH,FR,SA,SU,START,END,SCORE,ACCOUNT,ROOM,AUTHOR,MESSAGE});
   if(map[Settings::DISPLAY_NAME].isNull())
       return false;
   name=map[Settings::DISPLAY_NAME].toString();
   active=configBoolDefaultTrue(ACTIVE,map);
   mo=configBoolDefaultTrue(MO,map);
   tu=configBoolDefaultTrue(TU,map);
   we=configBoolDefaultTrue(WE,map);
   th=configBoolDefaultTrue(TH,map);
   fr=configBoolDefaultTrue(FR,map);
   sa=configBoolDefaultTrue(SA,map);
   su=configBoolDefaultTrue(SU,map);
   if(map[START].isNull()) {
       start.setHMS(0,0,0);
   }else
        start=map[START].toTime();
   if(map[END].isNull())
       end.setHMS(0,0,0);
   else
       end=map[END].toTime();
   accountRe.setPattern(map[ACCOUNT].toString());
   roomRe.setPattern(map[ROOM].toString());
   authorRe.setPattern(map[AUTHOR].toString());
   messageRe.setPattern(map[MESSAGE].toString());
   score=map[SCORE].toLongLong();
   return true;
}

bool Filter::setActive(bool active)
{
    this->active=active;
    return Settings::getInstance().setFilter(xmppAccount,name,ACTIVE,active);
}

bool Filter::setScore(qint64 score)
{
    this->score=score;
    return Settings::getInstance().setFilter(xmppAccount,name,SCORE,score);
}

bool Filter::setStart(QTime start)
{
    this->start=start;
    return Settings::getInstance().setFilter(xmppAccount,name,START,start);

}

bool Filter::setEnd(QTime end)
{
    this->end=end;
    return Settings::getInstance().setFilter(xmppAccount,name,END,end);

}

bool Filter::setMo(bool on)
{
    this->mo=on;
    return Settings::getInstance().setFilter(xmppAccount,name,MO,mo);
}

bool Filter::setTu(bool on)
{
    this->tu=on;
    return Settings::getInstance().setFilter(xmppAccount,name,TU,tu);
}

bool Filter::setWe(bool on)
{
    this->we=on;
    return Settings::getInstance().setFilter(xmppAccount,name,WE,we);

}

bool Filter::setTh(bool on)
{
    this->th=on;
    return Settings::getInstance().setFilter(xmppAccount,name,TH,th);

}

bool Filter::setFr(bool on)
{
    this->fr=on;
    return Settings::getInstance().setFilter(xmppAccount,name,FR,fr);

}

bool Filter::setSa(bool on)
{
    this->sa=on;
    return Settings::getInstance().setFilter(xmppAccount,name,SA,sa);

}

bool Filter::setSu(bool on)
{
    this->su=on;
    return Settings::getInstance().setFilter(xmppAccount,name,SU,su);
}
