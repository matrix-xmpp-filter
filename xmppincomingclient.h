#ifndef XMPPINCOMINGCLIENT_H
#define XMPPINCOMINGCLIENT_H

#include <QObject>
#include<QXmppPingIq.h>
#include<QDateTime>

class XmppIncomingClient : public QObject
{
    Q_OBJECT
public:
    explicit XmppIncomingClient(QObject *parent = nullptr);

signals:

public slots:

public:
    void setJid(const QString& jid);
    /**
     * @brief addConnection increments connectionCount
     * @return number of connections after add.
     */
    quint16 addConnection();
    /**
     * @brief removeConnection decrements connectionCount
     * @return number of connections after remove
     */
    quint16 removeConnection();
    quint16 getConnectionCount();

    void ping(QXmppPingIq&);


private:
    QString jid;
    QDateTime pingSent;
    quint16 connectionCount;

};

#endif // XMPPINCOMINGCLIENT_H
