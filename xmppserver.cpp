#include "logging.h"
#include "xmppserver.h"
#include <QXmppMessage.h>
#include <QXmppPingIq.h>
#include <QDomElement>
#include <QXmppPingIq.h>
#include "settings.h"

XmppServer::XmppServer()
{
    logger.setLoggingType(QXmppLogger::StdoutLogging);
    domain=Settings::getInstance().getXmppDomain();
}

// <message xmlns="jabber:client" from="user@domain/Jolla" to="matrix_room_sort_of_name@domain"
//  id="f01c6700-c3ce-442a-b3f8-bfc8bc2323" type="chat">
// <body xmlns="jabber:client">message goes herer</body>
// <active xmlns="http://jabber.org/protocol/chatstates"/>
// </message>
bool XmppServer::handleStanza(const QDomElement &stanza)
{
    QString str;
    QTextStream ts(&str);
    stanza.save(ts,0);
    QString type=stanza.attribute(QStringLiteral("type"));
    QString fromJid=stanza.attribute(QStringLiteral("from"));
    if(type==QStringLiteral("result")) {
        qCDebug(XMPP) << "from" << fromJid << "result id ="<< stanza.attribute(QStringLiteral("id"));
    }else if(type==QStringLiteral("chat")) {
        qCDebug(XMPP) << "xmppserver: handleStanza:" << str;
        emit fromXmpp(
            fromJid,
            stanza.attribute(QStringLiteral("to")),
            stanza.text()
            );
        return true;
    }
    return false;
}


void XmppServer::startServer()
{
    QString key=Settings::getInstance().getXmppPrivateKey();
    QString cert=Settings::getInstance().getXmppLocalCert();
    QString ca=Settings::getInstance().getXmppCa();
    quint16 xmppPort=Settings::getInstance().getXmppPort();
    if(xmppPort==0)
        xmppPort=5222;
    if(key.isNull()) {
        qCDebug(CONFIG) << "ERROR: xmpp server private key not set. See matrix-xmpp-filter -h";
        abort();
    }
    if(cert.isNull()) {
        qCDebug(CONFIG) << "ERROR: xmpp server certificate key not set. See matrix-xmpp-filter -h";
        abort();
    }
    QFile keyFile(key);
    if(!keyFile.exists()) {
        qCDebug(CONFIG) << "key file" << keyFile << " missing";
        abort();
    }
    QFile certFile(cert);
    if(!certFile.exists()) {
        qCDebug(CONFIG) << "cert file" << certFile << " missing";
        abort();
    }
    domain=Settings::getInstance().getXmppDomain();
    if(domain.isNull()) {
        qCDebug(CONFIG) << "ERROR: xmpp server domain is not set. See matrix-xmpp-filter -h";
        abort();
    }

    connect(&server,&QXmppServer::clientConnected, this, &XmppServer::clientConnected);
    connect(&server,&QXmppServer::clientDisconnected, this, &XmppServer::clientDisconnected);

    pingTimer.setInterval(55000);
    pingTimer.setSingleShot(true);
    pingTimer.start();

    connect(&pingTimer,&QTimer::timeout,this,&XmppServer::sendPings);

    server.setPrivateKey(key);
    server.setLocalCertificate(cert);
    if(!ca.isNull()) {
        server.addCaCertificates(ca);
    }
    server.setDomain(domain);
    server.setLogger(&logger);
    server.setPasswordChecker(&checker);
    server.listenForClients(QHostAddress::Any,xmppPort);
    server.addExtension(this);

}

bool XmppServer::toXmpp(const QString& fromJid,const QString &recipient,const QString &message)
{
    qCDebug(XMPP) << "server: sending " << fromJid << message;
    QXmppMessage m(fromJid, recipient, message);
    bool ok=server.sendPacket(m);
    if(!ok) {
        toXmppQueue[recipient].push_back(QPair<QString,QString>(fromJid,message));
        qCDebug(XMPP) << "ERROR sending from" << fromJid << "to" << recipient << ": " << message;
        if(toXmppQueue[recipient].size() > 20) {
            qCDebug(XMPP) << "Removing oldest message from queue";
            toXmppQueue[recipient].pop_front();
        }
    }
    return ok;
}

void XmppServer::clientConnected(const QString &jid)
{
    qCDebug(XMPP) << jid << "connected";
    XmppIncomingClient * c;
    if(clients.contains(jid)) {
        c=clients[jid];
    }else{
        c=new XmppIncomingClient;
        c->setJid(jid);
        clients[jid]=c;
    }
    c->addConnection();
    QString j;
    int index=jid.indexOf('/');
    if(index==-1) {
        j=jid;
    }else{
        j=jid.mid(0,index);
    }
    resendJids.append(j);
    QTimer::singleShot(2000, this, &XmppServer::resendToXmpp);
    newConnection(jid);
}

void XmppServer::resendToXmpp() {
    for(QString j:resendJids) {
        qCDebug(XMPP) << "resending"<< toXmppQueue[j].size() <<  "messages to" << j;
        while(!toXmppQueue[j].isEmpty()) {
            qCDebug(XMPP) << "resending to " << j;
            bool ok=toXmpp(toXmppQueue[j].front().first,j,toXmppQueue[j].front().second);
            toXmppQueue[j].pop_front();
            if(!ok) {
                qCDebug(XMPP) << "resending failed";
                break;
            }
        }
    }
    resendJids.clear();
}

void XmppServer::clientDisconnected(const QString &jid)
{
    auto c=clients[jid];
    if(c->getConnectionCount()==0) {
        clients.remove(jid);
        c->deleteLater();
    };
    qCDebug(XMPP) << jid << "disconnected";
}

void XmppServer::sendPings()
{
    qCDebug(XMPP) << "sending" << clients.size() << "pings";
    for(auto c:clients.values()) {
        QXmppPingIq p;
        p.setFrom(domain);
        c->ping(p);
        qCDebug(XMPP) << "sending xmpp ping to" << p.to();
        bool ok=server.sendPacket(p);
        if(ok) {
            qCDebug(XMPP) << "ping id" << p.id();
        }else{
            qCDebug(XMPP) << "pinging failed";
        }
    }
    pingTimer.start();
}
