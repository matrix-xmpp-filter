#ifndef XMPPPASSWORDCHECKER_H
#define XMPPPASSWORDCHECKER_H
#include "QXmppPasswordChecker.h"

#include <QObject>

class XmppPasswordChecker : public QObject, public QXmppPasswordChecker
{
    Q_OBJECT
public:
    explicit XmppPasswordChecker(QObject *parent = nullptr);
    QXmppPasswordReply::Error getPassword(const QXmppPasswordRequest &request, QString &password);
    bool hasGetPassword() const;

signals:

public slots:
};

#endif // XMPPPASSWORDCHECKER_H
