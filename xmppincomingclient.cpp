#include "xmppincomingclient.h"

XmppIncomingClient::XmppIncomingClient(QObject *parent) : QObject(parent)
{
    connectionCount=0;
    pingSent.setSecsSinceEpoch(0);
}

void XmppIncomingClient::setJid(const QString &jid)
{
    this->jid=jid;
}

quint16 XmppIncomingClient::addConnection()
{
    return ++connectionCount;
}

quint16 XmppIncomingClient::removeConnection()
{
    return --connectionCount;
}

quint16 XmppIncomingClient::getConnectionCount()
{
    return connectionCount;
}

void XmppIncomingClient::ping(QXmppPingIq& p)
{
    p.setTo(jid);
}

