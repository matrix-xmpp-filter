#ifndef FORWARDER_H
#define FORWARDER_H

#include<qxmpp/QXmppClient.h>
#include<qxmpp/QXmppMessage.h>

#include <QtCore/QCoreApplication>
#include <QtCore/QStringBuilder>
#include <QtCore/QTimer>
#include <QSettings>
#include <iostream>
#include <functional>
#include <QTimer>
#include <QPair>


#include "matrix.h"
#include "filter.h"
#include "xmppserver.h"
#include "xmpp.h"

class Forwarder : public QObject
{
Q_OBJECT
public:
    Forwarder();
    void start(bool xmppServer=true);
    /**
     * @brief matrixOutgoingTransactions key is libQuotient transaction id, value is pair of fromjid, tojid
     */
    QMap<QString, QPair<QString,QString>> matrixOutgoingTransactions;
    /**
     * @brief xmppServer act as xmppServer
     */
    XmppServer *xs;

public slots:
    void matrixMessageSentServerMode(QString txnId, QString eventId);
    void messageFromXmppServerMode(const QString & fromJid, const QString& toJid, const QString& message);
    void newXmppConnection(const QString& jid);

private:
//	QXmppConfiguration conf;
	//QXmppPresence pre;


    QMap<Matrix *, Xmpp *> xmppsByMatrix;
    QMap<QString, Xmpp *> xmppsByAccountName;
    QMap<QString, Xmpp *> xmppsByJid;
public:
    int xmppRetries;
    bool xmppServer;
private:

    const Xmpp * getXmpp(Matrix * m);

    Xmpp * getXmpp(const QString& jid);

    void startAccount(const QString& account);
    void startAccounts();
    void stopAccount(const QString& account);
};

#endif // FORWARDER_H
