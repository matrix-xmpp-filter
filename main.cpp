#include <QCoreApplication>
#include<QAbstractSocket>
#include <qxmpp/QXmppClient.h>
#include "forwarder.h"
#include "logging.h"
#include <QTimer>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include "settings.h"

QString readPass() {
    QTextStream in(stdin);
    QString pass(in.readLine());
    if(pass.endsWith(QStringLiteral("\n"))) {
        pass.remove(pass.length()-1,1);
        if(pass.endsWith(QStringLiteral("\r"))) {
            pass.remove(pass.length()-1,1);
        }
    }

    return pass;
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("matrix-xmpp-filter");
    QCoreApplication::setOrganizationName("matrix-xmpp-filter");
    QCoreApplication::setApplicationVersion("0.1");

    if(argc > 1) {
        QTextStream out(stdout);
        QCommandLineParser parser;
        QCommandLineOption xmppAccount(QStringLiteral("a"), QCoreApplication::translate("main","Choose xmpp account to modify"), QStringLiteral("myXmppAccount"));
        QCommandLineOption displayName(QStringLiteral("n"),QStringLiteral("Choose matrix account to modify"),QStringLiteral("myMatrixAccount"));
        QCommandLineOption setJid(QStringLiteral("j"), QCoreApplication::translate("main","Set jabber id"),"puppet@foo.example");
        QCommandLineOption setRecipient(QStringLiteral("r"),QCoreApplication::translate("main","Set xmpp recipient for client mode"), "alice@foo.example");
        QCommandLineOption setJpass(QStringLiteral("p"),QStringLiteral("Read xmpp password from stdin"));
        QCommandLineOption list(QStringLiteral("l"),QStringLiteral("List accounts"));
        QCommandLineOption matrixUserId(QStringLiteral("m"),QStringLiteral("Set matrix userId"), QStringLiteral("@alice:foo.example") );
        QCommandLineOption matrixAccessToken(QStringLiteral("t"),QStringLiteral("Set matrix accesstoken"),QStringLiteral("VudGlmaWVyIGtleQ"));
        QCommandLineOption matrixDeviceId(QStringLiteral("d"),QStringLiteral("Set matrix deviceId"),QStringLiteral("EIFCKWDFIFJE"));
        QCommandLineOption matrixHomeserver(QStringLiteral("s"),QStringLiteral("Set matrix homeserver url"),QStringLiteral("https://foo.example"));
        QCommandLineOption syncLoopTimeout(QStringLiteral("S"),QStringLiteral("sync loop timeout. Negative=use as loop interval, positive=sync loop timeout"),QStringLiteral("60000"));
        QCommandLineOption filterCommand(QStringLiteral("f"),QStringLiteral("filter command"),QStringLiteral("..."));
        QCommandLineOption listFilters(QStringLiteral("F"),QStringLiteral("list filters"));
        QCommandLineOption xmppDomain(QStringLiteral("D"),QStringLiteral("set xmpp server domain"),QStringLiteral("foo.example"));
        QCommandLineOption xmppPort(QStringLiteral("L"),QStringLiteral("set xmpp port number"),QStringLiteral("5222"));
        QCommandLineOption xmppPrivateKey(QStringLiteral("P"),QStringLiteral("set path of xmpp ssl private key file to config file"),QStringLiteral("/path/to/privkey.pem"));
        QCommandLineOption xmppCA(QStringLiteral("A"),QStringLiteral("set path of xmpp intermediate CA cert file to config file"),QStringLiteral("/path/to/chain.pem"));
        QCommandLineOption xmppLocalCert(QStringLiteral("C"),QStringLiteral("set path of xmpp ssl certificate file to config file"),QStringLiteral("/path/to/cert.pem"));
        QCommandLineOption clientMode(QStringLiteral("c"),QStringLiteral("Toggle client mode in settings"));
        QCommandLineOption dbHost(QStringLiteral("dbhost"),QStringLiteral("Set database host to config file"),QStringLiteral("dbserver.example.com"));
        QCommandLineOption dbPort(QStringLiteral("dbport"),QStringLiteral("Set database port to config file"),QStringLiteral("5432"));
        QCommandLineOption dbUser(QStringLiteral("dbuser"),QStringLiteral("Set database username to config file"),QStringLiteral("username"));
        QCommandLineOption dbPass(QStringLiteral("dbpassword"),QStringLiteral("Set database password to config file"),QStringLiteral("password"));
        QCommandLineOption dbName(QStringLiteral("dbname"),QStringLiteral("Set database name to config file"),QStringLiteral("database"));
        parser.addOption(xmppAccount);
        parser.addOption(setJid);
        parser.addOption(setRecipient);
        parser.addOption(setJpass);
        parser.addOption(list);
        parser.addOption(matrixUserId);
        parser.addOption(matrixAccessToken);
        parser.addOption(matrixDeviceId);
        parser.addOption(matrixHomeserver);
        parser.addOption(displayName);
        parser.addOption(syncLoopTimeout);
        parser.addOptions({filterCommand,listFilters});
        parser.addOptions({xmppLocalCert,xmppPrivateKey,xmppDomain,xmppPort,xmppCA});
        parser.addOptions({dbHost,dbPort,dbUser,dbPass,dbName});
        parser.addOption(clientMode);
        parser.addHelpOption();
        parser.process(a);
        typedef void(Settings::*QStringSettingConsumer)(const QString&);
        QMap<QCommandLineOption *, QStringSettingConsumer> settables;
        settables[&dbHost]=&Settings::setDbHost;
        settables[&dbUser]=&Settings::setDbUser;
        settables[&dbPass]=&Settings::setDbPass;
        settables[&dbName]=&Settings::setDbName;
        bool needsync=false;
        for(auto ite=settables.begin(); ite != settables.end(); ite++) {
            QCommandLineOption * opt=ite.key();
            QStringSettingConsumer func=ite.value();
            if(parser.isSet(*opt)) {
                qCDebug(CONFIG) << opt->names() << "is set with value" << parser.value(*opt);
                (Settings::getInstance().*func)(parser.value(*opt));
                needsync=true;
            }
        }
        if(parser.isSet(dbPort)) {
            Settings::getInstance().setDbPort(static_cast<quint16>(parser.value(dbPort).toInt()));
            needsync=true;
        }
        QString account;
        if(parser.isSet(xmppAccount)) {
            account=parser.value(xmppAccount);
            qCDebug(CONFIG) << "account=" << account;
        }else{
            qCDebug(CONFIG) << "no xmpp account on command line";
        }
        if(parser.isSet(setJid)) {
            Settings::getInstance().setJid(account,parser.value(setJid));
            qCDebug(CONFIG) << "jid set to" << parser.value(setJid);
            needsync=true;
        }
        if(parser.isSet(setJpass)) {
            qCDebug(CONFIG) << "reading xmpp password from stdin";
            QTextStream out(stdout);
            out << "reading xmpp password from stdin\n";
            out.flush();
            Settings::getInstance().setJpass(account,readPass());
            needsync=true;
            //qCDebug(CONFIG) << "pass=" << pass;
        }
        if(parser.isSet(setRecipient)) {
            QString value=parser.value(setRecipient);
            Settings::getInstance().setRecipient(account,value);
            needsync=true;
            qCDebug(CONFIG) << "xmpp recipient set to" << value;
        }
        if(parser.isSet(matrixUserId) && parser.isSet(matrixHomeserver) && parser.isSet(displayName)) {
                if(parser.isSet(matrixAccessToken)) {
                    if(parser.isSet(matrixDeviceId)) {
                        bool success=Settings::getInstance().addMatrixToken(account,parser.value(displayName),parser.value(matrixUserId),parser.value(matrixAccessToken),parser.value(matrixDeviceId), parser.value(matrixHomeserver));
                        if(!success) {
                            qCDebug(CONFIG) << "failed to add matrix account with access token";
                            return 3;
                        }
                        needsync=true;
                    }else{
                        out << "ERROR: device id is needed with accessToken";
                        return 4;
                    }
                }else{
                    out << "reading matrix password from stdin\n";
                    out.flush();
                    Settings::getInstance().addMatrixPassword(account,parser.value(displayName),parser.value(matrixUserId),readPass(),parser.value(matrixHomeserver),parser.value(matrixDeviceId));
                    qCDebug(CONFIG) << "event loop start";
                    return a.exec();
                }
        }else if(parser.isSet(matrixUserId) || parser.isSet(matrixHomeserver)) {
            out << "need all three options: matrix account name, matrix user id and matrix homeserver\n";
        }
        if(parser.isSet(list)) {
            QTextStream out(stdout);
            Settings::getInstance().listMatrix(account);
        }
        if(parser.isSet(syncLoopTimeout)) {
            int timeout=parser.value(syncLoopTimeout).toInt();
            Settings::getInstance().setSyncLoopTimeout(timeout);
            needsync=true;
        }
        if(parser.isSet(filterCommand)) {
            out << "TODO\n";
            /*
            Forwarder f;
            Settings::getInstance().fillFilterList(account,f.filterList);
            QString cmd=parser.value(filterCommand);
            QString result=f.filterCommand(account,cmd);
            out << result;
            out << "\n";
            */
        }
        if(parser.isSet(listFilters)) {
            out << Settings::getInstance().listFilters();
            out << "\n";
        }
        if(parser.isSet(xmppDomain)) {
            Settings::getInstance().setXmppDomain(parser.value(xmppDomain));
            needsync=true;
        }
        if(parser.isSet(xmppLocalCert)) {
            Settings::getInstance().setXmppLocalCert(parser.value(xmppLocalCert));
            needsync=true;
        }
        if(parser.isSet(xmppPrivateKey)) {
            Settings::getInstance().setXmppPrivateKey(parser.value(xmppPrivateKey));
            needsync=true;
        }
        if(parser.isSet(xmppCA)) {
            Settings::getInstance().setXmppCA(parser.value(xmppCA));
            needsync=true;
        }
        if(parser.isSet(clientMode)) {
            bool state=Settings::getInstance().toggleClientMode();
            out << "client_mode=" << state << "\n";
            needsync=true;
        }
        if(parser.isSet(xmppPort)) {
            Settings::getInstance().setXmppPort(static_cast<quint16>(parser.value(xmppPort).toUInt()));
            needsync=true;
        }
        if(needsync)
            Settings::getInstance().sync();
        return 0;
    }else{
        if(!Settings::getInstance().dbConnected) {
            qCCritical(CONFIG) << "Database not connected. You might need to set dbname to config file with --dbname";
            abort();
        }
    }
    
	Forwarder f;

    f.start(!Settings::getInstance().isClientMode());
	return a.exec();
}
