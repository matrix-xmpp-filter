#include "xmpp.h"
#include "forwarder.h"
#include "logging.h"
#include "settings.h"
#include <stringprep.h>
#include <string.h>
#include<QRegularExpression>


#define OK QStringLiteral("OK")

Xmpp::Xmpp(QObject *parent) : QObject(parent)
{
    replyRoom=nullptr;
    replyMatrix=nullptr;
    matrixConnected=false;
}

void Xmpp::setXmppRecipient(const QString &jid)
{
    xmppRecipient=jid;
}

void Xmpp::clientMode() {
        xc=new QXmppClient;
        QObject::connect(xc,&QXmppClient::messageReceived,this,&Xmpp::messageFromXmppClientMode);
        QObject::connect(xc,&QXmppClient::error,this,&Xmpp::xmppError);
        QObject::connect(xc,&QXmppClient::connected,this,&Xmpp::xmppConnected);
        QObject::connect(xc,&QXmppClient::disconnected,this,&Xmpp::xmppDisconnected);
}

void Xmpp::disconnectXmpp()
{
    if(xc) {
        QObject::disconnect(xc,&QXmppClient::messageReceived,this,&Xmpp::messageFromXmppClientMode);
        QObject::disconnect(xc,&QXmppClient::error,this,&Xmpp::xmppError);
        QObject::disconnect(xc,&QXmppClient::connected,this,&Xmpp::xmppConnected);
        QObject::disconnect(xc,&QXmppClient::disconnected,this,&Xmpp::xmppDisconnected);
        QObject::connect(xc,&QXmppClient::disconnected,this,[this] () {
            qCDebug(XMPP) << account << "xmpp disconnected";
            xc->deleteLater();
            this->deleteLater();
        });
        xc->disconnectFromServer();
    }
}


void Xmpp::connectToServer(const QString &jid, const QString &pass)
{
    xc->connectToServer(jid,pass);
}

void Xmpp::xmppDisconnected()
{
    qCDebug(XMPP) << QStringLiteral("xmpp disconnected");
    xmppReconnect();
}

void Xmpp::xmppError(QXmppClient::Error error)
{
    qCDebug(XMPP) << QStringLiteral("xmpp error:") << error;
    xmppReconnect();
}

void Xmpp::xmppConnected()
{
    qCDebug(XMPP) << QStringLiteral("xmpp connected");
    if(!matrixConnected)
        connectMatrix();
}

void Xmpp::messageFromXmppClientMode(const QXmppMessage &message)
{
    const QString msg=message.body();
    if(msg.isEmpty()) {
        qCDebug(XMPP) << "empty from xmpp:" << message.type();
        return;
    }
    qCDebug(XMPP) << "from xmpp:" << msg;
    QString reply;
    if(msg.startsWith(QLatin1String(" /"))) {
        reply=command(msg.mid(2));
    }else if(msg.startsWith(QLatin1String("/"))) {
        reply=command(msg.mid(1));
    }else{
        if(replyRoom!=nullptr && replyMatrix!=nullptr) {
            QString txnId=replyRoom->postPlainText(msg);
            forwarder->matrixOutgoingTransactions[txnId]=QPair<QString,QString>(message.from(),message.to());
            return;
        }else{
            reply=QStringLiteral("No reply room set.");
        }
    }
    xc->sendMessage(xmppRecipient,reply);
}

void Xmpp::connectMatrix() {
    Settings::getInstance().fillMatrixList(account,matrixList);
    Settings::getInstance().fillFilterList(account,filterList);
    if(matrixList.isEmpty()) {
        qCDebug(XMPP) << "WARING: xmpp user" << account << "does not have any matrix account configured";
        return;
    }
    for(Matrix *m : matrixList) {
        connect(m, &Matrix::forward, this, &Xmpp::matrixMessage);
        connect(m, &Matrix::roomConnected, this, &Xmpp::addRoom);
    }

    int timeout=Settings::getInstance().getSyncLoopTimeout();
    if(timeout < 0) {
        timeout=-timeout;
        int size=matrixList.size();
        syncTimer.setSingleShot(true);
        syncTimer.setInterval(timeout);
        connect(&syncTimer,&QTimer::timeout, matrixList.at(0),&Matrix::syncNow);
        for(int i=1; i < size ; i++) {
            connect(matrixList.at(i-1), &Matrix::syncDoneDone, matrixList.at(i), &Matrix::syncNow);
        }
        connect(matrixList.at(size-1),&Matrix::syncDoneDone,this,[this,timeout]() {
            qCDebug(MATRIX) << "start sync timer";
            syncTimer.setSingleShot(true);
            syncTimer.setInterval(timeout);
            syncTimer.start();
        });
        syncTimer.start();
    }
    matrixConnected=true;
}

void Xmpp::matrixMessage(Matrix *m, Room* room, const QString& author, const QString&msg)
{
    QString roomName=room->displayName();
    qint64 score=0;
    qCDebug(FILTER) << filterList.size()  << "filters";
    for(Filter *f:filterList) {
        qCDebug(FILTER) << "filter:"<< f->getName();
        score+=f->getScore(m->name(), roomName, author, msg);
    }
    qCDebug(FILTER) << "score =" << score << m->name() << room << author << msg;
    if(score >=0) {

        if(forwarder->xmppServer) {
            forwarder->xs->toXmpp(getJidWithRoom(room), xmppRecipient, QStringLiteral("<")+author+QStringLiteral("> ")+msg);
        }else if(previousAccount!=m->name()) {
            xc->sendMessage(xmppRecipient,m->name()+":"+roomName+"<"+author+"> "+msg);
            previousAccount=m->name();
            previousRoom=roomName;
            previousAuthor=author;
        }else if (previousRoom!=roomName) {
            xc->sendMessage(xmppRecipient,roomName+":"+author+": "+msg);
            previousRoom=roomName;
            previousAuthor=author;
        }else if (previousAuthor!=author) {
            xc->sendMessage(xmppRecipient,author+": "+msg);
            previousAuthor=author;
        }else {
             xc->sendMessage(xmppRecipient,msg);
        }
    }
}

void Xmpp::matrixMessageSentClientMode(QString txnId, QString eventId)
{
    qCDebug(MATRIX) << "SENT" << eventId;
    xc->sendMessage(xmppRecipient,QStringLiteral("SENT"));
    forwarder->matrixOutgoingTransactions.remove(txnId);
}

QString Xmpp::listRooms(const QString &arg)
{
    QString ret;
    QString account=arg.trimmed();
    bool all=account.isEmpty();
    ret.reserve(4096);
    bool found=false;
    if(matrixList.isEmpty()) {
        return QStringLiteral("No matrix accounts");
    }
    bool accountNames=all && matrixList.size() > 1;
    for(Matrix *ma:matrixList) {
        if(!all && ma->name()!=account) {
            continue;
        }
        found=true;
        QStringList rooms=ma->roomNames();
        int n=rooms.size();
        for(int i=0 ; i < n; i++) {
            if(accountNames) {
                ret.append(ma->name());
                ret.append(QStringLiteral(" "));
            }
            ret.append(rooms.at(i));
            ret.append("\n");
        }
    }
    if(!all && !found) {
        ret.append(QStringLiteral("Account not found"));
    }
    return ret;
}

QString Xmpp::command(const QString &command)
{
    qCDebug(COMMAND) <<"command"<< command;
    if(command.startsWith(QLatin1String("room "))) {
        return setXmppRoom(command.mid(5));
    }else if(command==QLatin1String("r")) {
        return setXmppRoom(QStringLiteral());
    }else if(command==QLatin1String("R")) {
        return getXmppRoom();
    }else if(command.startsWith(QLatin1String("rooms"))) {
        return listRooms(command.mid(5));
    }else if(command.startsWith(QLatin1String("filter "))) {
        return filterCommand(command.mid(7));
    }else if(command==QLatin1String("exit")) {
        QTimer::singleShot(1000,this,[](){
            QCoreApplication::exit();
        });
        return QStringLiteral("exiting");
    }else if(command==QLatin1String("help")) {
        return QStringLiteral("\n /filter help -- show filter usage\n"
                              " /rooms [account] -- list rooms \n"
                              " /room [account] room_name -- set reply room\n"
                              " /r -- set reply room from previous message\n"
                              " /R -- query current reply room\n"
                              " /exit -- exit after 1 second\n"
                              );
    }
    return QStringLiteral("command not understood");
}

/**
 * @brief Forwarder::filterCommand
 * @param command
 * @return
 *
 * Filter command syntax:
 *
 * /filter help
 * /filter list
 * /filter <name> show
 * /filter <name> [active=0|1] [score=N] [start=HH:MM] [end=HH:MM] [mo=0|1] [tu=0|1] [we=0|1] [th=0|1] [fr=0|1] [sa=0|1] [su=0|1]
 * /filter <name> re <account|room|author|message> <regular_expression>
 * /filter <name> delete
*/
QString Xmpp::filterCommand(const QString &command)
{
    bool ok=false;
    qCDebug(COMMAND) <<"filter command"<< command;
    if(command==QLatin1String("list")) {
        return Settings::getInstance().listFilters();
    }else if(command==QLatin1String("help")) {
        return QLatin1String("/filter help\n"
                             "/filter list\n"
                             "/filter <name> show\n"
                             "/filter <name> [active=0|1] [score=N] [start=HH:MM] [end=HH:MM] [mo=0|1] [tu=0|1] [we=0|1] [th=0|1] [fr=0|1] [sa=0|1] [su=0|1]\n"
                             "/filter <name> re <account|room|author|message> <regular_expression>\n"
                             "/filter <name> delete\n"
                             );
    }

    QStringList splitted=command.split(QStringLiteral(" "),QString::SkipEmptyParts);
    if(splitted.size() < 2) {
        return QStringLiteral("ERROR");
    }
    QString name=splitted.at(0);
    if(splitted.at(1)==QStringLiteral("show")) {
        Filter *f=getFilter(name,false);
        if(f==nullptr) {
            return QStringLiteral("filter ")+name+QStringLiteral(" not found");
        }else{
            return f->toString();
        }
    }
    if(splitted.at(1)==QStringLiteral("delete")) {
        if(!Settings::getInstance().removeFilter(account,name)) {
            return "ERROR";
        }
        for(int i=0 ; i < filterList.length(); i++) {
            if(filterList.at(i)->getName()==name) {
                filterList.removeAt(i);
                return OK;
            }
        }
        return "Not found";
    }
    if(splitted.at(1)==QStringLiteral("re")) {
        if(splitted.size() < 4) {
            return QStringLiteral("ERROR. Usage: <name> re <account|room|author|message> <regular_expression>");
        }
        QRegularExpression re(QStringLiteral("^\\s*\\S+\\s+re\\s+(account|room|author|message)\\s+(.*)$"));
        QRegularExpressionMatch match=re.match(command);
        if(!match.hasMatch()) {
            return QStringLiteral("ERROR");
        }
        QString reName=match.captured(1);
        QString regexp=match.captured(2);
        qCDebug(COMMAND) << reName << regexp;
        Filter *f=getFilter(name);
        if(reName==QStringLiteral("account")) {
            ok=f->setAccountRe(regexp);
        }else if(reName==QStringLiteral("room")) {
            ok=f->setRoomRe(regexp);
        }else if(reName==QStringLiteral("author")) {
            ok=f->setAuthorRe(regexp);
        }else if(reName==QStringLiteral("message")) {
            ok=f->setMessageRe(regexp);
        }
        if(ok)
            return OK;
        else
            return "ERROR";
    }
    QString reply;
    for(int i=1 ; i < splitted.size(); i++) {
        if(!splitted.at(i).contains(QLatin1String("="))) {
            reply.append(QStringLiteral("bad:"));
            reply.append(splitted.at(i));
            reply.append(QStringLiteral("\n"));
            continue;
        }
        QStringList change=splitted.at(i).split(QStringLiteral("="));
        Filter *f=getFilter(name);
        if(change.at(0)==QStringLiteral("active")) {
            ok=f->setActive(change.at(1).toInt()==1);
        }else if (change.at(0)==QStringLiteral("score")) {
            ok=f->setScore(change.at(1).toLongLong());
        }else if (change.at(0)==QStringLiteral("start")) {
            ok=f->setStart(QTime::fromString(change.at(1)));
        }else if (change.at(0)==QStringLiteral("end")) {
            ok=f->setEnd(QTime::fromString(change.at(1)));
        }else if (change.at(0)==QStringLiteral("mo")) {
            ok=f->setMo(change.at(1).toInt()==1);
        }else if (change.at(0)==QStringLiteral("tu")) {
            ok=f->setTu(change.at(1).toInt()==1);
        }else if (change.at(0)==QStringLiteral("we")) {
            ok=f->setWe(change.at(1).toInt()==1);
        }else if (change.at(0)==QStringLiteral("th")) {
            ok=f->setTh(change.at(1).toInt()==1);
        }else if (change.at(0)==QStringLiteral("fr")) {
            ok=f->setFr(change.at(1).toInt()==1);
        }else if (change.at(0)==QStringLiteral("sa")) {
            ok=f->setSa(change.at(1).toInt()==1);
        }else if (change.at(0)==QStringLiteral("su")) {
            ok=f->setSu(change.at(1).toInt()==1);
        }else{
            reply.append(QStringLiteral("bad:"));
            reply.append(splitted.at(i));
            reply.append(QStringLiteral("\n"));
        }
        if(ok) {
            return OK;
        }
    }
    return QStringLiteral("No such filter command.");
}

void Xmpp::xmppReconnect()
{
    if(++forwarder->xmppRetries > 100)
        QCoreApplication::exit();
    else {
        connectToServer(Settings::getInstance().getJid(account),Settings::getInstance().getJpass(account));
    }
}

Room *Xmpp::getRoomWithJid(const QString &jid)
{
    qCDebug(XMPP) << "getRoomWithJid" << jid;
    if(!jidRooms.contains(jid)) {
        qCWarning(XMPP) << "No room with jid" << jid <<"found.";
        return nullptr;
    }
    return jidRooms[jid];
}


QString Xmpp::getJidWithRoom(Room *room)
{
    if(room==nullptr) {
       qCDebug(XMPP) << "getJidWithRoom(nullptr) called!";
       abort();
    }
    QString suffix=QStringLiteral("@")+Settings::getInstance().getXmppDomain();
    if(!roomJids.contains(room->id())) {
        QString node=nodePrep(room);
        QString jid=node+suffix;
        if(jidRooms.contains(jid)) {
            //collision
            int i=2;
            while(jidRooms.contains(node+QString::number(i)+suffix)) {
                i++;
            }
            jid=node+QString::number(i)+suffix;
        }
        jidRooms[jid]=room;
        roomJids[room->id()]=jid;
        qCDebug(XMPP) << "using " << jid << "with room" << room->id() << "=" << room->displayName();
    }
    return roomJids[room->id()];
}

void Xmpp::disconnectMatrix()
{
    for(Matrix * m:matrixList) {
        m->disconnectMatrix();
    }
}

QString Xmpp::nodePrep(const Room *room)
{
    QString s=room->displayName();
    s
            .replace(QLatin1String(" "),QLatin1String("_"))
            .replace(QLatin1String("\""),QLatin1String("_"))
            .replace(QLatin1String("&"),QLatin1String("_and_"))
            .replace(QLatin1String("'"),QLatin1String("_"))
            .replace(QLatin1String("/"),QLatin1String("|"))
            .replace(QLatin1String(":"),QLatin1String("."))
            .replace(QLatin1String("<"),QLatin1String("(("))
            .replace(QLatin1String(">"),QLatin1String("))"))
            .replace(QLatin1String("@"),QLatin1String("(at)"))
            ;

    Stringprep_profile_flags flags;
    memset(&flags,0,sizeof(Stringprep_profile_flags));
    QByteArray array=s.toUtf8();
    array.resize(array.size()+128);
    size_t maxlen=static_cast<size_t>(array.size());
    int result=stringprep(array.data(),maxlen,flags,stringprep_xmpp_nodeprep);
    if(result==STRINGPREP_OK) {
        return QString(array);
    }
    qCDebug(MISC) << "Error with nodePrep. stringPrep returned " << result << "for" << s << "using room id instead";

    s=room->id();
    s.replace(QLatin1String("@"),QLatin1String("(at)"));
    array=s.toUtf8();
    array.resize(array.size()+128);
    maxlen=static_cast<size_t>(array.size());
    result=stringprep(array.data(),maxlen,flags,stringprep_xmpp_nodeprep);
    if(result==STRINGPREP_OK) {
        return QString(array);
    }

    qCDebug(MISC) << "failed to nodeprep matrix room id. This is a bug!";
    return s.mid(1,s.indexOf(':'));

}

void Xmpp::addRoom(Room *room)
{
    //Need to fill roomJids and jidRooms maps before chat messages come from xmpp.
    //This avoids "Room not found" error.
    getJidWithRoom(room);

    if(forwarder->xmppServer)
        QObject::connect(room,&Room::messageSent,forwarder,&Forwarder::matrixMessageSentServerMode);
    else
        QObject::connect(room,&Room::messageSent,this,&Xmpp::matrixMessageSentClientMode);
}

Filter *Xmpp::getFilter(const QString &name, bool createIfNotFound)
{
    for(Filter *f:filterList) {
        if(f->getName()==name) {
            return f;
        }
    }
    if(createIfNotFound) {
        qCDebug(CONFIG) << "new Filter";
        Filter *f=Settings::getInstance().getFilter(this,name);
        filterList.append(f);
        return f;
    }
    return nullptr;
}

QString Xmpp::getXmppRoom()
{
    if(replyMatrix==nullptr || replyRoom==nullptr) {
        return QStringLiteral("reply room is not set");
    }
    return replyMatrix->name()+QStringLiteral(" ")+replyRoom->displayName();
}

QString Xmpp::setXmppRoom(const QString &value)
{
    if(value.isEmpty()) {
        return setXmppRoom(previousAccount+QStringLiteral(" ")+previousRoom);
    }
    QString trimmed=value.trimmed();
    QStringList values=trimmed.split(QStringLiteral(" "));
    QString acc, roo;
    Room *r=nullptr;
    Matrix *m=nullptr;
    if(values.size() > 1) {
        m=getMatrix(values.at(0));
        if(m!=nullptr) {
            acc=values.at(0);
            QRegularExpression lastWords(QStringLiteral("^\\S+\\s+(.*)$"));
            QRegularExpressionMatch match=lastWords.match(trimmed);
            roo=match.captured(1);
        }else{
            roo=trimmed;
        }
    }else{
        roo=trimmed;
    }
    if(acc.isEmpty()) {
        for(Matrix *ma:matrixList) {
            r=ma->getRoom(roo);
            if(r!=nullptr) {
                m=ma;
                break;
            }
        }
    }else{
        m=getMatrix(acc);
        if(m==nullptr) {
            return "Matrix "+acc+" not found";
        }
        r=m->getRoom(roo);
    }
    if(r==nullptr)
        return QStringLiteral("room not found");
    replyMatrix=m;
    replyRoom=r;
    if(acc.isEmpty()) {
        return QStringLiteral("Replies go to ")+replyRoom->displayName();
    }
    return QStringLiteral("Replies go to ")+replyMatrix->name()+QStringLiteral(" ")+replyRoom->displayName();
}

Matrix *Xmpp::getMatrix(const QString &name)
{
    for(Matrix *ma:matrixList) {
        if(ma->name()==name) {
            return ma;
        }
    }
    return nullptr;
}
