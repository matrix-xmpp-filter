#ifndef XMPPSERVER_H
#define XMPPSERVER_H
#include <QXmppLogger.h>
#include <QXmppServer.h>
#include <QLinkedList>
#include <QTimer>
#include <QXmppServerExtension.h>
#include "xmpppasswordchecker.h"
#include "xmppincomingclient.h"

#include <QObject>

class XmppServer : public QXmppServerExtension
{
    Q_OBJECT
public:
    explicit XmppServer();
    bool handleStanza(const QDomElement &stanza);

signals:
    void fromXmpp(const QString& from, const QString& to, const QString & messsage);
    void newConnection(const QString& jid);

public slots:
    void startServer();
    bool toXmpp(const QString& fromJid ,const QString& recipient, const QString& message);
    void clientConnected(const QString &jid);
    void clientDisconnected(const QString &jid);
    void sendPings();

private:
    QXmppLogger logger;
    QXmppServer server;
    XmppPasswordChecker checker;
    QString domain;
    /**
     * @brief toXmppQueue key is destination jid, value is list of fromJid,messageText pairs
     */
    QMap<QString,QLinkedList<QPair<QString,QString>>> toXmppQueue;
    /**
     * @brief connectionCount key is incoming client jid
     */
    QMap<QString,XmppIncomingClient *> clients;
    QList<QString> resendJids;
    QTimer pingTimer;

private slots:
    void resendToXmpp();
};

#endif // XMPPSERVER_H
