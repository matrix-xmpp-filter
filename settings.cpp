#include "logging.h"
#include "settings.h"
#include "matrix.h"
#include <QTextStream>
#include <QDebug>
#include <QCoreApplication>
#include <QStringBuilder>
#include <QVariant>

const QString Settings::MATRIX_USER(QStringLiteral("matrix_user"));
//#define MTX QStringLiteral("matrix")
#define USER_ID QStringLiteral("userId")
const QString Settings::ACCESS_TOKEN(QStringLiteral("access_token"));
const QString Settings::DEVICE_ID(QStringLiteral("deviceid"));
const QString Settings::HOMESERVER(QStringLiteral("homeserver"));
const QString Settings::DISPLAY_NAME(QStringLiteral("display_name"));
const QString Settings::XMPP_CA(QStringLiteral("xmppCa"));
#define XMPP_RECIPIENT QStringLiteral("xmpprecipient")
#define JID QStringLiteral("jid")
#define JPASS QStringLiteral("pass")
#define SYNC_LOOP_TIMEOUT QStringLiteral("syncLoopTimeout")
#define FILTERS QStringLiteral("filters")
#define XMPP_DOMAIN QStringLiteral("xmppDomain")
#define XMPP_LOCAL_CERT QStringLiteral("xmppLocalCert")
#define XMPP_PRIVATE_KEY QStringLiteral("xmppPrivKey")
#define CLIENT_MODE QStringLiteral("clientMode")
#define XMPP_PORT QStringLiteral("xmppPort")
#define XMPP_ACCOUNTS QStringLiteral("xmppAccounts")
#define XMPP_ACCOUNT QStringLiteral("xmppAccount")
#define DBUSER QStringLiteral("dbUser")
#define DBPASS QStringLiteral("dbPass")
#define DBHOST QStringLiteral("dbHost")
#define DBNAME QStringLiteral("dbName")
#define DBPORT QStringLiteral("dbPort")

Settings::Settings(QObject *parent) : QObject(parent)
{
    qCDebug(CONFIG) << "config file is" << qsettings.fileName();
    QSqlDatabase::addDatabase("QSQLITE");
    dbConnected=connectDb();
}

bool Settings::connectDb() {
    auto host=qsettings.value(DBHOST);
    auto port=qsettings.value(DBPORT);
    auto user=qsettings.value(DBUSER);
    auto pass=qsettings.value(DBPASS);
    auto name=qsettings.value(DBNAME);
    QSqlDatabase db=QSqlDatabase::database();
    if(!host.isNull())
        db.setHostName(host.toString());
    if(!name.isNull()) {
        qCDebug(CONFIG) << "db name:" << name.toString();
        db.setDatabaseName(name.toString());
    }
    if(!user.isNull())
        db.setUserName(name.toString());
    if(!pass.isNull())
        db.setPassword(pass.toString());
    if(!port.isNull())
        db.setPort(port.toInt());
    bool connected = db.open();
    if(!connected) {
        qCWarning(DATABASE) << "couldn't connect to database" << db.lastError();
    }
    if(connected) {
        if(!addDbSchema())
            return false;
    }
    return connected;
}

Filter *Settings::getFilter(Xmpp *xmpp, const QString &name)
{
    auto filter_id=getFilter(getXmppId(xmpp->account),name);
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare(QStringLiteral("SELECT name,active,mo,tu,we,th,fr,sa,su,start,end,accountre,roomre,authorre,messagere,score FROM filter WHERE id = :i"));
    q.bindValue(QStringLiteral(":i"),filter_id);
    if(!q.exec()) {
        qCWarning(DATABASE) << "getFilter exec" << db.lastError();
        db.rollback();
        return nullptr;
    }
    if(!q.next()) {
        qCWarning(DATABASE) << "getFilter next" << db.lastError();
        db.rollback();
        return nullptr;
    }
    auto f=new Filter(xmpp->account, q.value(0),q.value(1),q.value(2),q.value(3),q.value(4),q.value(5),
                      q.value(6),q.value(7),q.value(8),q.value(9),q.value(10),
                      q.value(11),q.value(12),q.value(13),q.value(14),q.value(15));
    db.rollback();
    return f;
}

bool Settings::addDbSchema() {
    QSqlDatabase db=QSqlDatabase::database();
    if(!db.tables().isEmpty()) {
        return true;
    }
    db.transaction();
    auto dbname=qsettings.value(DBNAME);
    if(dbname.isNull()) {
        return false;
    }
    qCDebug(DATABASE) << "Empty database \"" << dbname << "\". Adding tables.";
    db.exec("CREATE TABLE xmpp (id integer PRIMARY KEY, account_name text UNIQUE NOT null, jid text, pass text)");
    db.exec("CREATE TABLE matrix ("
            "id integer PRIMARY KEY, "
            "xmpp_id integer REFERENCES xmpp(id) NOT null, "
            "display_name text NOT null,"
            "homeserver text,"
            "matrix_user text, "
            "access_token text,"
            "deviceid text)");
    db.exec("CREATE TABLE room (id integer PRIMARY KEY, matrix_id integer REFERENCES matrix(id) NOT null, last_forwarded text)");
    db.exec("CREATE TABLE filter ("
           "id integer PRIMARY KEY,"
            "xmpp_id integer REFERENCES xmpp(id) NOT null,"
            "name text NOT null,"
            "active boolean,"
            "mo boolean,"
            "tu boolean,"
            "we boolean,"
            "th boolean,"
            "fr boolean,"
            "sa boolean,"
            "su boolean,"
            "start time,"
            "end time,"
            "accountre text,"
            "roomre text,"
            "authorre text,"
            "messagere text,"
            "score bigint"
            ")");
    qCDebug(DATABASE) << db.tables().size() << "tables";
    db.commit();
    return true;
}

int Settings::getXmppId(const QString &account, bool createIfNeeded)
{
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare("SELECT id FROM xmpp WHERE account_name=:n");
    q.bindValue(QStringLiteral(":n"),account);
    bool ok=q.exec();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return -1;
    }
    bool found=q.next();
    if(found) {
        int id=q.value(0).toInt();
        db.rollback();
        return id;
    }
    if(!createIfNeeded) {
        qCWarning(CONFIG) << "did not find" << account;
        return -2;
    }
    QSqlQuery u;
    u.prepare("INSERT INTO xmpp (account_name) VALUES (:n)");
    u.bindValue(QStringLiteral(":n"),account);
    ok=u.exec();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return -1;
    }
    ok=db.commit();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return -1;
    }
    return getXmppId(account,false);
}

int Settings::getMatrixIdWithDisplayName(int xmpp_id, const QString &displayName,bool createIfNeeded)
{
    return getMatrixId(xmpp_id,QStringLiteral("display_name"), displayName,createIfNeeded);
}

int Settings::getMatrixIdWithUserId(int xmpp_id, const QString &userId)
{
    return getMatrixId(xmpp_id,MATRIX_USER,userId,false);
}
int Settings::getMatrixId(int xmpp_id, const QString &key,const QString& value,bool createIfNeeded)
{
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare("SELECT id FROM matrix WHERE xmpp_id=:x AND "
              +key+
              "=:n");
    q.bindValue(QStringLiteral(":x"),xmpp_id);
    q.bindValue(QStringLiteral(":n"),value);
    bool ok=q.exec();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return -1;
    }
    bool found=q.next();
    if(found) {
        int id=q.value(0).toInt();
        db.rollback();
        return id;
    }
    if(!createIfNeeded || QStringLiteral("display_name") != key) {
        qCWarning(DATABASE) << "did not find matrix account of xmpp account" << xmpp_id << "with" << key << "=" << value;
        return -2;
    }
    QSqlQuery u;
    u.prepare("INSERT INTO matrix (xmpp_id,display_name) VALUES (:x,:n)");
    u.bindValue(QStringLiteral(":x"),xmpp_id);
    u.bindValue(QStringLiteral(":n"),value);
    ok=u.exec();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return -1;
    }
    ok=db.commit();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return -1;
    }
    return getMatrixIdWithDisplayName(xmpp_id,value,false);
}

QString Settings::getJid(const QString& account)
{
    auto jid=getXmppAccountValue(account,JID);
    qCDebug(CONFIG) << "jid="<<jid;
    if(jid.isNull()){
        qCWarning(CONFIG) << "ERROR: jid is null in account" << account;
    }
    return jid;
}

QString Settings::getXmppAccountValue(const QString &account, const QString &key) {
    auto db=QSqlDatabase::database();
    db.transaction();
    QString query=QStringLiteral("SELECT ")+key+QStringLiteral(" FROM xmpp WHERE account_name=:n");
    qCDebug(DATABASE) << query;
    QSqlQuery q;
    q.prepare(query);
    q.bindValue(QStringLiteral(":n"),account);
    bool ok=q.exec();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return nullptr;
    }
    if(q.next()) {
        const QString result= q.value(0).toString();
        db.rollback();
        return result;
    }
    qCDebug(CONFIG) << "getXmppAccountValue: no results for" << account << key << "size=" << q.size();
    db.rollback();
    return nullptr;

    /*
    int index=getXmppAccountIndex(account);
    if(index==-1)
        return nullptr;
    qsettings.beginReadArray(XMPP_ACCOUNTS);
    qsettings.setArrayIndex(index);
    const QString& result=qsettings.value(key).toString();
    endArrayFilter();
    return result;
    */
}
bool Settings::setMatrixAccountValue(const QString& xmpp_account,const QString& matrix_account,const QString&key,const QString&value) {
    if(xmpp_account.isNull()) {
        qCWarning(CONFIG) << "setMatrixAccountValue: xmpp account is null";
        return false;
    }
    if(matrix_account.isNull()) {
        qCWarning(CONFIG) << "setMatrixAccountValue: matrix account is null";
        return false;
    }
    if(key.isNull()) {
        qCWarning(CONFIG) << "setMatrixAccountValue: key is null";
        return false;
    }
    int xmpp_id=getXmppId(xmpp_account,false);
    if(xmpp_id < 0) {
        qCWarning(CONFIG) << "setMatrixAccountValue: no xmpp account found with" << xmpp_account;
        return false;
    }
    int matrix_id=getMatrixIdWithDisplayName(xmpp_id,matrix_account);
    if(matrix_id < 0) {
        qCWarning(CONFIG) << "no matrix account found with xmpp_id " << xmpp_id << " and matrix account name " << matrix_account;
        return false;
    }
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare("UPDATE matrix SET "+key+"=:v WHERE id=:i");
    q.bindValue(QStringLiteral(":v"),value);
    q.bindValue(QStringLiteral(":i"),matrix_id);
    bool ok=q.exec();
    if(!ok) {
        qCWarning(DATABASE) << "setMatrixAccountValue failed: " << db.lastError();
        db.rollback();
        return false;
    }
    ok=db.commit();
    if(!ok) {
        qCWarning(DATABASE) << "setMatrixAccountValue failed to commit: " << db.lastError();
        db.rollback();
        return false;
    }
    return true;
}
bool Settings::setXmppAccountValue(const QString &account, const QString &key, const QString &value) {
    if(account.isNull()) {
        qCWarning(CONFIG) << "account is null";
    }
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare(QStringLiteral("SELECT id FROM xmpp WHERE account_name = :n"));
    q.bindValue(QStringLiteral(":n"),account);
    bool ok=q.exec();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return false;
    }
    bool found=q.next();
    qCDebug(DATABASE) << q.executedQuery();
    qCDebug(CONFIG) << "found ="<< found;
    QSqlQuery u;
    if(found) {
        u.prepare(QStringLiteral("UPDATE xmpp SET ")+key+QStringLiteral("=:value WHERE id=:id"));
        u.bindValue(QStringLiteral(":value"),value);
        u.bindValue(QStringLiteral(":id"),q.value(0).toInt());
    }else{
        u.prepare(QStringLiteral("INSERT INTO xmpp (account_name,")+key+QStringLiteral(") VALUES (:a,:v)"));
        u.bindValue(QStringLiteral(":a"),account);
        u.bindValue(QStringLiteral(":v"),value);
    }
    ok=u.exec();
    qCDebug(DATABASE) << u.executedQuery();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return false;
    }
    ok=db.commit();
    if(!ok) {
        qCWarning(DATABASE) << db.lastError();
        db.rollback();
        return false;
    }
    return true;


    /*
    int index=getXmppAccountIndex(account);
    if(index==-1)
        return false;
    qsettings.beginWriteArray(XMPP_ACCOUNTS);
    qsettings.setArrayIndex(index);
    qsettings.setValue(key,value);
    qsettings.endArray();
    return true;
    */
}

void Settings::beginFilterGroup(const QString &account)
{
    QString group=FILTERS+QStringLiteral("_")+account;
    qsettings.beginGroup(group);
}

QString Settings::getJpass(const QString& account)
{
    return getXmppAccountValue(account,JPASS);
}

void Settings::setJid(const QString& account,const QString &jid)
{
    setXmppAccountValue(account,JID,jid);
}

void Settings::setRecipient(const QString& account,const QString & r)
{
    setXmppAccountValue(account,XMPP_RECIPIENT,r);
}

QString Settings::getRecipient(const QString& account)
{
    return getXmppAccountValue(account,XMPP_RECIPIENT);
}

void Settings::setJpass(const QString& account,const QString &jpass)
{
    setXmppAccountValue(account,JPASS,jpass);
}

Settings &Settings::getInstance()
{
    static Settings instance;
    return instance;
}

void Settings::listMatrix(const QString& account)
{
    QTextStream out(stdout);
    auto db=QSqlDatabase::database();
    db.transaction();
    QMap<int,QString> xmpp_ids;
    bool ok;
    if(account.isNull()) {
        QSqlQuery q;
        q.prepare("SELECT id,account_name FROM xmpp");
        ok=q.exec();
        if(!ok) {
            out << "ERROR: " << db.lastError().text() << "\n";
            db.rollback();
            return;
        }
        while(q.next()) {
            xmpp_ids[q.value(0).toInt()]=q.value(1).toString();
        }
    }else{
        xmpp_ids[getXmppId(account)]=account;
    }
    for(auto ite=xmpp_ids.begin(); ite != xmpp_ids.end() ; ite++) {
        const int xmpp_id=ite.key();
        const QString account_name=ite.value();
        out << "XMPP account \"" << account_name << "\"\n";
        QSqlQuery q;
        q.prepare("SELECT display_name,homeserver,matrix_user,access_token,deviceid FROM matrix WHERE xmpp_id=:x");
        q.bindValue(QStringLiteral(":x"),xmpp_id);
        ok=q.exec();
        if(ok) {
            q.size();
            out << q.size() << " matrix accounts\n";
            while(q.next()) {
                out << "displayName:" << q.value(0).toString() << "\n";
                out << "homeserver:" << q.value(1).toString() << "\n";
                out << "userId: " << q.value(2).toString() << "\n";
                QString accessToken=q.value(3).toString();
                out << "accessToken: " << accessToken.length() << " characters.\n";
                out << "deviceId:" << q.value(4).toString() << "\n";
                out << "\n";
            }
        }else{
            out << "ERROR: " << db.lastError().text() << "\n";
        }
        out << "\n";
    }
    db.rollback();
    //TODO
    /*
    qsettings.beginReadArray(MTX);
    auto indices=countArray();
    QTextStream out(stdout);
    int really=0;
    for(int i:indices) {
        qsettings.setArrayIndex(i);
        if(!qsettings.value(USER_ID).isNull()) {
            auto av=qsettings.value(XMPP_ACCOUNT);
            if(!av.isNull()) {
                QString xmppAccount=av.toString();
                if(account==xmppAccount) {
                    really++;
                }
            }
        }
    }
    out << account << " has " << really << " matrix accounts\n";
    if(really > 0)
        out << "\n";
    for(int i=0; i < really ; i++) {
        qsettings.setArrayIndex(i);
        QVariant userId=qsettings.value(USER_ID);
        if(!userId.isNull()) {
            out << "displayName:" << qsettings.value(DISPLAY_NAME).toString() << "\n";
            out << "homeserver:" << qsettings.value(HOMESERVER).toString() << "\n";
            out << "userId: " << userId.toString() << "\n";
            out << "accessToken:" << qsettings.value(ACCESS_TOKEN).toString() << "\n";
            out << "deviceId:" << qsettings.value(DEVICE_ID).toString() << "\n";
            out << "\n";
        }
    }
    qsettings.endArray();

    out << "\nxmpp id: " << getJid(account) << "\n";
    QVariant jpass = qsettings.value(JPASS);
    if(jpass.isNull()) {
        out << "xmpp password is NOT set\n";
    }else{
        out << "xmpp password is ********\n";
    }
    out << "xmpp recipient: " << getRecipient(account) << "\n";

    QVariant xmppPort=qsettings.value(XMPP_PORT);
    if(!xmppPort.isNull()) {
        out << "xmpp port: " << xmppPort.toString() << "\n";
    }
    */
}

void Settings::addMatrixPassword(const QString& account,const QString &displayName, const QString &userId,
                                 const QString &password, const QString& homeserver, const QVariant& deviceId)
{
    bool ok=setMatrixAccountValue(account,displayName,MATRIX_USER,userId);
    if(!ok) {
        qCWarning(CONFIG) << "failed to set " << MATRIX_USER;
        return;
    }
    ok=setMatrixAccountValue(account,displayName,HOMESERVER,homeserver);
    if(!ok) {
        qCWarning(CONFIG) << "failed to set " << HOMESERVER;
        return;
    }
    if(!deviceId.isNull()) {
        ok=setMatrixAccountValue(account,displayName,DEVICE_ID,deviceId.toString());
        if(!ok) {
            qCWarning(CONFIG) << "failed";
            return;
        }
    }

    Matrix *m=new Matrix(QStringLiteral("login"), getSyncLoopTimeout());
    connect(m, &Matrix::accessTokenCreated, this, [this, account, displayName, userId, m, homeserver] () {
        addMatrixToken(account,displayName, userId, m->accessToken(), m->deviceId(), homeserver);
        m->disconnectMatrix();
        QCoreApplication::quit();
    });
    m->createAccessToken(userId,password,homeserver,deviceId);
}

bool Settings::addMatrixToken(const QString& account,const QString &displayName, const QString &userId, const QString &accessToken, const QString &deviceId, const QString& homeserver)
{
    qCDebug(CONFIG) << "addMatrixToken";
    if(!accessToken.isNull()) {
        bool ok=setMatrixAccountValue(account,displayName,ACCESS_TOKEN,accessToken);
        if(!ok) {
            qCWarning(CONFIG) << "addMatrixToken fail1";
        }
    }
    if(!deviceId.isNull()) {
        bool ok=setMatrixAccountValue(account,displayName,DEVICE_ID,deviceId);
        if(!ok) {
            qCWarning(CONFIG) << "addMatrixToken fail2";
        }
    }
    if(!homeserver.isNull()) {
        bool ok=setMatrixAccountValue(account,displayName,HOMESERVER,homeserver);
        if(!ok) {
            qCWarning(CONFIG) << "addMatrixToken fail3";
        }
    }
    if(!userId.isNull()) {
        bool ok=setMatrixAccountValue(account,displayName,MATRIX_USER,userId);
        if(!ok) {
            qCWarning(CONFIG) << "addMatrixToken fail4";
        }
    }
    return true;
}

bool Settings::removeMatrix(const QString& account,const QString &userId)
{
    int xmpp_id=getXmppId(account);
    int matrix_id=getMatrixIdWithUserId(xmpp_id,userId);
    if(matrix_id < 0) {
        //not found
        return true;
    }

    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare("DELETE FROM matrix WHERE id=:i");
    q.bindValue(QStringLiteral(":i"),matrix_id);
    bool ok=q.exec();
    if(!ok) {
        qCWarning(CONFIG) << "ERROR: delete:" << db.lastError();
        db.rollback();
        return false;
    }
    ok=db.commit();
    if(!ok) {
        qCWarning(CONFIG) << "ERROR: commit:" << db.lastError();
        return false;
    }
    return true;
}

void Settings::sync()
{
    qsettings.sync();
}

void Settings::setSyncLoopTimeout(int timeout)
{
    qsettings.setValue(SYNC_LOOP_TIMEOUT,timeout);
}

int Settings::getSyncLoopTimeout()
{
    QVariant to=qsettings.value(SYNC_LOOP_TIMEOUT,-20000);
    return to.toInt();
}

QString Settings::listFilters(const QString& account)
{
    QString result;
    int xmpp_id;
    if(account.isNull())
        xmpp_id=-1;
    else
        xmpp_id=getXmppId(account);
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    if(xmpp_id < 0)
        q.prepare("SELECT "
              "name,active,mo,tu,we,th,fr,sa,su,start,end,accountre,roomre,authorre,messagere,score "
              "FROM filter");
    else{
        q.prepare("SELECT "
              "name,active,mo,tu,we,th,fr,sa,su,start,end,accountre,roomre,authorre,messagere,score "
              "FROM filter "
              "WHERE xmpp_id=:x");
        q.bindValue(QStringLiteral(":x"),xmpp_id);
    }
    bool ok=q.exec();
    if(!ok) {
        qCDebug(DATABASE) << "listFilters failed to select" << db.lastError();
        db.rollback();
        return result;
    }
    int size=q.size();
    if(size > 0)
        result.reserve(size*200);
    while(q.next()) {
            Filter f(account,
                    q.value(0), q.value(1), q.value(2), q.value(3), q.value(4),
                    q.value(5), q.value(6), q.value(7), q.value(8), q.value(9),
                    q.value(10), q.value(11), q.value(12), q.value(13),
                    q.value(14), q.value(15));
            result.append(f.toString());
            result.append(QStringLiteral("\n"));
    }
    db.rollback();
    return result;

    /*
    // beginReadArray returns the stored size which might be wrong.
    // therefore we find existing array indexes the hard way
    qsettings.beginReadArray(FILTERS);
    QList<int> arrayIndices=countArray();
    int size=arrayIndices.size();
    QString result;
    result.reserve(size*200);
    if(size > 1)
        result.append(QStringLiteral("\n"));
    for(int i:arrayIndices) {
        qsettings.setArrayIndex(i);
        QVariant name=qsettings.value(DISPLAY_NAME);
        if(!name.isNull()) {
            Filter f;
            f.read();
            result.append(f.toString());
            result.append(QStringLiteral("\n"));
        }
    }
    qsettings.endArray();
    return result;
    */
}

bool Settings::setFilter(const QString& account,const QString &filter_name, const QString &key, const QVariant &value)
{
    int xmpp_id=getXmppId(account);
    int filter_id=getFilter(xmpp_id,filter_name);
    if(filter_id < 0) {
        return false;
    }
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    QString query="UPDATE filter SET "+key+"=:v WHERE xmpp_id=:x AND name=:n";
    qCDebug(DATABASE) << query << "|v="<<value<<"x="<<xmpp_id<<"n="<<filter_name ;
    q.prepare(query);
    q.bindValue(QStringLiteral(":v"),value);
    q.bindValue(QStringLiteral(":x"),xmpp_id);
    q.bindValue(QStringLiteral(":n"),filter_name);
    bool ok=q.exec();
    if(!ok) {
        qCDebug(DATABASE) << "setFilter failed to update:" << db.lastError();
        db.rollback();
        return false;
    }
    ok=db.commit();
    if(!ok) {
        qCDebug(DATABASE) << "setFilter failed to commit:" << db.lastError();
        db.rollback();
        return false;
    }
    return true;

    /*
    beginArrayWriteFilter(account,filter);
    qsettings.setValue(key,value);
    endArrayFilter();
    */
}

int Settings::getFilter(int xmpp_id, const QString &filter_name, bool createIfNeeded) {
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare("SELECT id FROM filter WHERE xmpp_id=:x AND name=:n");
    q.bindValue(QStringLiteral(":x"),xmpp_id);
    q.bindValue(QStringLiteral(":n"),filter_name);
    bool ok=q.exec();
    if(!ok) {
        qCDebug(DATABASE) << "getFilter failed:" << db.lastError();
        db.rollback();
        return -1;
    }
    bool found=q.next();
    if(found) {
        int filter_id=q.value(0).toInt();
        db.rollback();
        return filter_id;
    }
    if(!createIfNeeded) {
        db.rollback();
        return -2;
    }
    QSqlQuery u;
    u.prepare("INSERT INTO filter (xmpp_id,name,active,score) VALUES (:x,:n,true,0)");
    u.bindValue(QStringLiteral(":x"),xmpp_id);
    u.bindValue(QStringLiteral(":n"),filter_name);
    ok=u.exec();
    if(!ok) {
        qCDebug(DATABASE()) << "getFilter failed to insert:" << db.lastError();
        db.rollback();
        return -3;
    }
    ok=db.commit();
    if(!ok) {
        qCDebug(DATABASE) << "getFilter failed to commit:" << db.lastError();
        db.rollback();
        return -4;
    }
    return getFilter(xmpp_id,filter_name,false);
}

/*
QVariantMap Settings::getFilter(const QString& account,const QString& filter, const QList<QString> &keys)
{
    QVariantMap map;
    bool needEndArray;
    if(filter!=QStringLiteral("")) {
        int index=beginArrayReadFilter(account,filter);
        needEndArray=true;
        if(index==-1)
            return map;
    }else {
        needEndArray=false;
    }
    for(QString key:keys) {
        QVariant value=qsettings.value(key);
        map[key]=value;
    }
    if(needEndArray)
        qsettings.endArray();
    return map;
}
*/

bool Settings::removeFilter(const QString& account,const QString &filter_name)
{
    int xmpp_id=getXmppId(account,false);
    if(xmpp_id < 0)
        return true;
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    const QString query(QStringLiteral("DELETE FROM filter WHERE xmpp_id=:x AND name=:n"));
    qCDebug(DATABASE) << query << "|x="<<xmpp_id<<"n="<<filter_name;
    q.prepare(query);
    q.bindValue(QStringLiteral(":x"),xmpp_id);
    q.bindValue(QStringLiteral(":n"),filter_name);
    bool ok=q.exec();
    if(!ok) {
        qCDebug(DATABASE) << "removeFilter failed to delete:" << db.lastError();
        db.rollback();
        return false;
    }
    ok=db.commit();
    if(!ok) {
        qCDebug(DATABASE) << "removeFilter failed to commit:" << db.lastError();
        db.rollback();
        return false;
    }
    return true;

    /*
    beginArrayWriteFilter(account,filter);
    auto keys=qsettings.allKeys();
    for(auto key:keys) {
        qsettings.remove(key);
    }
    endArrayFilter();
    */
}

void Settings::setXmppDomain(const QString &s)
{
    qsettings.setValue(XMPP_DOMAIN, s);
}

void Settings::setXmppLocalCert(const QString &s)
{
    qsettings.setValue(XMPP_LOCAL_CERT, s);
}

void Settings::setXmppPrivateKey(const QString &s)
{
    qsettings.setValue(XMPP_PRIVATE_KEY, s);
}

void Settings::setXmppCA(const QString &ca)
{
    qsettings.setValue(XMPP_CA, ca);
}

QString Settings::getXmppDomain()
{
    return qsettings.value(XMPP_DOMAIN).toString();
}

QString Settings::getXmppLocalCert()
{
    return qsettings.value(XMPP_LOCAL_CERT).toString();
}

QString Settings::getXmppPrivateKey()
{
    return qsettings.value(XMPP_PRIVATE_KEY).toString();
}

QString Settings::getXmppCa()
{
    return qsettings.value(XMPP_CA).toString();
}

bool Settings::toggleClientMode()
{
    bool ret;
    qsettings.setValue(CLIENT_MODE,ret=!qsettings.value(CLIENT_MODE).toBool());
    return ret;
}

bool Settings::isClientMode()
{
    return qsettings.value(CLIENT_MODE).toBool();
}

quint16 Settings::getXmppPort()
{
    return quint16(qsettings.value(XMPP_PORT).toUInt());
}

void Settings::setXmppPort(quint16 port)
{
    qsettings.setValue(XMPP_PORT, port);
}

const QString Settings::getJpassWithUsername(const QString &username)
{
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare("SELECT pass FROM xmpp WHERE jid LIKE :j");
    q.bindValue(QStringLiteral(":j"),username+QStringLiteral("@%"));
    bool ok=q.exec();
    if(!ok) {
        qCDebug(DATABASE) << "getJpassWithUsername failed to select" << db.lastError();
        db.rollback();
        return nullptr;
    }
    if(!q.next()) {
        qCDebug(DATABASE) << "getJpassWithUsername no password found";
        db.rollback();
        return nullptr;
    }
    auto pass=q.value(0).toString();
    db.rollback();
    return pass;

    /*
    qsettings.beginReadArray(XMPP_ACCOUNTS);
    auto indices=countArray();
    QString pass;
    for(int i:indices) {
        qsettings.setArrayIndex(i);
        if(qsettings.value(JID).toString().startsWith(username+QStringLiteral("@"))) {
            pass=qsettings.value(JPASS).toString();
            break;
        }
    }
    qsettings.endArray();
    return pass;
    */
}

QStringList Settings::xmppAccountNames()
{
    QStringList list;
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare("SELECT account_name FROM xmpp");
    bool ok=q.exec();
    if(!ok) {
        qCDebug(DATABASE) << "xmppAccountNames fail:" << db.lastError();
        db.rollback();
        return list;
    }
    while(q.next()) {
        list.append(q.value(0).toString());
    }
    db.rollback();
    return list;

    /*
    qsettings.beginReadArray(XMPP_ACCOUNTS);
    auto indices=countArray();
    QStringList list;
    for(int i:indices) {
        qsettings.setArrayIndex(i);
        list.append(qsettings.value(DISPLAY_NAME).toString());
    }
    qsettings.endArray();
    return list;
    */
}

void Settings::setDbHost(const QString & host)
{
    qsettings.setValue(DBHOST, host);
}

void Settings::setDbUser(const QString &user)
{
    qsettings.setValue(DBUSER, user);
}

void Settings::setDbPass(const QString &pass)
{
    qsettings.setValue(DBPASS, pass);
}

void Settings::setDbName(const QString &name)
{
    qCDebug(CONFIG) << "new db name:" << name;
    qsettings.setValue(DBNAME, name);
}

void Settings::setDbPort(const quint16 port)
{
    qsettings.setValue(DBPORT, port);
}

void Settings::fillMatrixList(const QString& account,QList<Matrix *> &list)
{
    int to=getSyncLoopTimeout();
    int xmpp_id=getXmppId(account);
    auto db=QSqlDatabase::database();
    QSqlQuery q;
    q.prepare("SELECT display_name,homeserver,matrix_user,access_token,deviceid FROM matrix WHERE xmpp_id=:x");
    q.bindValue(QStringLiteral(":x"),xmpp_id);
    bool ok=q.exec();
    if(!ok) {
        qCDebug(MATRIX) << "fillMatrixList failed to select" << db.lastError();
        db.rollback();
        return;
    }
    while(q.next()) {
        Matrix *m=new Matrix(q.value(0).toString(),to);
        list.push_back(m);
        m->connectToMatrix(q.value(1).toString(),
                           q.value(2).toString(),
                           q.value(3).toString(),
                           q.value(4).toString());
    }
    db.rollback();

    /*
    qsettings.beginReadArray(MATRIX);
    auto indices=countArray();
    for(int i:indices) {
        qsettings.setArrayIndex(i);
        QString u=qsettings.value(USER_ID).toString();
        if(u.isNull()) {
            continue;
        }
        auto av=qsettings.value(XMPP_ACCOUNT);
        if(av.isNull())
            continue;
        if(av.toString()!=account)
            continue;
        Matrix *m=new Matrix(qsettings.value(DISPLAY_NAME).toString(), to);
        list.push_back(m);
        m->connectToMatrix(qsettings.value(HOMESERVER).toString(),qsettings.value(USER_ID).toString(),qsettings.value(ACCESS_TOKEN).toString(),qsettings.value(DEVICE_ID).toString());
    }
    qsettings.endArray();
    */
}

void Settings::fillFilterList(const QString& account,QList<Filter *> &list)
{
    qCDebug(CONFIG) << "fillFilterList";
    int xmpp_id=getXmppId(account);
    auto db=QSqlDatabase::database();
    db.transaction();
    QSqlQuery q;
    q.prepare("SELECT "
              "name,active,mo,tu,we,th,fr,sa,su,start,end,accountre,roomre,authorre,messagere,score "
              "FROM filter "
              "WHERE xmpp_id=:x");
    q.bindValue(QStringLiteral(":x"),xmpp_id);
    bool ok=q.exec();
    if(!ok) {
        qCDebug(DATABASE) << "ERROR: fillFilterList failed to select:" << db.lastError();
        db.rollback();
        return;
    }
    while(q.next()) {
        Filter *f=new Filter(
                    account,
                    q.value(0),
                    q.value(1),
                    q.value(2),
                    q.value(3),
                    q.value(4),
                    q.value(5),
                    q.value(6),
                    q.value(7),
                    q.value(8),
                    q.value(9),
                    q.value(10),
                    q.value(11),
                    q.value(12),
                    q.value(13),
                    q.value(14),
                    q.value(15)
                    );
        list.append(f);
    }
    db.rollback();

    /*

    beginFilterGroup(account);
    qsettings.beginReadArray(FILTERS);
    auto indices=countArray();
    int size=indices.size();
    qCDebug(FILTER) << size << "filters in config";
    for(int i:indices) {
        qsettings.setArrayIndex(i);
        Filter *f=new Filter();
        if(f->read()) {
            qCDebug(FILTER) << "using filter"<< f->getName();
            list.append(f);
        }else{
            qCDebug(FILTER) << "NOT using filter at index" << i;
            f->deleteLater();
        }
    }
    qsettings.endArray();
    qsettings.endGroup();
    */
}


