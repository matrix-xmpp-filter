#ifndef MATRIX_H
#define MATRIX_H
#include <QtCore/QCoreApplication>
#include <QtCore/QStringBuilder>
#include <QtCore/QTimer>
#include <QtCore/QTemporaryFile>
#include <QtCore/QFileInfo>
#include<QTimer>
#include <iostream>
#include <functional>
#include <Quotient/connection.h>
#include <Quotient/room.h>
#include <Quotient/user.h>
#include <Quotient/csapi/room_send.h>
#include <Quotient/csapi/joining.h>
#include <Quotient/csapi/leaving.h>
#include <Quotient/jobs/downloadfilejob.h>
#include <Quotient/events/simplestateevents.h>
#include <Quotient/joinstate.h>

using namespace QMatrixClient;
/**
 * @brief The Matrix class contains libQuotient Connection and some helper functions
 */
class Matrix : public QObject
{
Q_OBJECT
public:
 Matrix(const QString & name, const int syncLoopTimeout);
 Room* getRoom(const QString&name);
 QStringList roomNames();
public slots:
  void connectToMatrix(const QString &homeserver, const QString &userId, const QString& accessToken, const QString& deviceId);
  void connectedToMatrix();
  void syncDone();
  QByteArray accessToken() const;
  QString deviceId() const;
  QString name() const;
  void disconnectMatrix();
  void createAccessToken(const QString& userId,const QString& password,const QString& homeserver,const QVariant & deviceId);
  void roomBaseStateLoaded();
  void connectRooms();
  void syncNow();
  void processMessage(Room *room, unsigned long i);

private:
  Connection c;
  void printRoomInfo(Room *);
  void printRoomInfo(const char * text, Room *);
  QString displayName;
  time_t start_time;
  unsigned long sync_count;
  int syncLoopTimeout;
  bool roomsConnected;
signals:
  void syncMatrix(int timeout=-1);
  void forward(Matrix* m, Room* room, QString author, QString message);
  void accessTokenCreated();
  void accessTokenCreateFailed();
  void syncDoneDone();
  void roomConnected(Room * room);

};
#endif // MATRIX_H
