#ifndef FILTER_H
#define FILTER_H

#include <QObject>
#include <QRegularExpression>
#include <QTime>
#include <QSettings>

class Filter : public QObject
{
    Q_OBJECT
public:
    explicit Filter(QObject *parent = nullptr);
    qint64 getScore(const QString &account, const QString&room, const QString&author, const QString&message);

signals:

public slots:

public:
    static const QString ACTIVE;
    static const QString ACCOUNTRE,AUTHORRE,MESSAGERE,ROOMRE;
    void setXmppAccount(const QString& xmppAccount);
    /**
     * @brief getName
     * @return filter name
     */
    QString getName();
    void setName(const QString& name);
    QString toString();
    /**
     * @brief setAccountRe
     * @param re reqular expression that is matched against matrix account nickname
     */
    bool setAccountRe(const QString& re);
    /**
     * @brief setRoomRe
     * @param re regular expression that is matched against matrix room displayname
     */
    bool setRoomRe(const QString& re);
    /**
     * @brief setAuthorRe
     * @param re regular expression that is matched against matrix message author (@username:domain.example)
     */
    bool setAuthorRe(const QString& re);
    /**
     * @brief setMessageRe
     * @param re regular expression that is matched against matrix message text
     */
    bool setMessageRe(const QString& re);
    /**
     * @brief read read config of this filter from map
     * @return
     */
    bool read(QVariantMap map);
    /**
     * @brief setActive set the filter active and save to settings
     * @param active
     */
    bool setActive(bool active);
    /**
     * @brief setScore set score given by this filter and save to settings
     * @param score
     */
    bool setScore(qint64 score);
    /**
     * @brief setStart set filter start time and save to settings
     * @param start
     */
    bool setStart(QTime start);
    /**
     * @brief setEnd set filter end time and save to settings
     * @param end
     */
    bool setEnd(QTime end);
    /**
     * @brief setMo set this filter enabled or disabled on mondays
     * @param on
     */
    bool setMo(bool on);
    bool setTu(bool on);
    bool setWe(bool on);
    bool setTh(bool on);
    bool setFr(bool on);
    bool setSa(bool on);
    bool setSu(bool on);

    Filter(QVariant account_name,
           QVariant filter_name,
           QVariant active,
           QVariant mo,
           QVariant tu,
           QVariant we,
           QVariant th,
           QVariant fr,
           QVariant sa,
           QVariant su,
           QVariant start,
           QVariant end,
           QVariant accountRe,
           QVariant roomRe,
           QVariant authorRe,
           QVariant messageRe,
           QVariant score);

private:
    QString xmppAccount;
    QString name;
    bool active;
    bool mo;
    bool tu;
    bool we;
    bool th;
    bool fr;
    bool sa;
    bool su;
    QTime start;
    /**
     * @brief end
     * end is exclusive. Therefore rule 00:00 - 23:59 will be valid all minutes of the day, but one.
     * To make the rule valid every minute of the day, set start==end
     * To make the rule not valid at any time, set all weekdays to false.
     */
    QTime end;

    QRegularExpression accountRe;
    QRegularExpression roomRe;
    QRegularExpression authorRe;
    QRegularExpression messageRe;

    qint64 score;
    /**
     * @brief configBoolDefaultTrue return boolean value from map or true if value doesn't exist
     */
    bool configBoolDefaultTrue(const QString &key, const QVariantMap &map);
    bool qvariantBoolDefaultTrue(const QVariant value);
};

#endif // FILTER_H
