#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QtSql>
#include "matrix.h"
#include "filter.h"
#include "xmpp.h"

class Settings : public QObject
{
    Q_OBJECT

public:
    explicit Settings(QObject *parent = nullptr);
    QString getJid(const QString& account);
    QString getJpass(const QString& account);
    void setJid(const QString& account,const QString&jid);
    void setRecipient(const QString& account,const QString&recipientJid);
    QString getRecipient(const QString& account);
    void setJpass(const QString& account,const QString&password);
    static Settings& getInstance();
    void listMatrix(const QString& account);
    void addMatrixPassword(const QString& account,const QString &displayname, const QString& userId, const QString& password,const QString& homeserver, const QVariant &deviceid);
    bool addMatrixToken(const QString& account,const QString &displayname, const QString& userId, const QString& accessToken, const QString& deviceId, const QString& homeserver);
    bool removeMatrix(const QString& account,const QString& userId);
    void sync();
    void setSyncLoopTimeout(int timeout);
    int getSyncLoopTimeout();
    QString listFilters(const QString& account=nullptr);
    bool setFilter(const QString& account,const QString& filter, const QString &key, const QVariant &value);
    //QVariantMap getFilter(const QString& account,const QString& filter, const QList<QString> &keys);
    int getFilter(int xmpp_id, const QString& filter_name, bool createIfNeeded=true);
    bool removeFilter(const QString& account,const QString& filter);
    void setXmppDomain(const QString&);
    void setXmppLocalCert(const QString&);
    void setXmppPrivateKey(const QString&);
    void setXmppCA(const QString &ca);
    QString getXmppDomain();
    QString getXmppLocalCert();
    QString getXmppPrivateKey();
    QString getXmppCa();
    bool toggleClientMode();
    bool isClientMode();
    quint16 getXmppPort();
    void setXmppPort(quint16);
    const QString getJpassWithUsername(const QString& username);
    QStringList xmppAccountNames();
    void setDbHost(const QString&);
    void setDbUser(const QString&);
    void setDbPass(const QString&);
    void setDbName(const QString&);
    void setDbPort(const quint16);
    bool connectDb();
    /**
     * @brief getFilter gets or creates a filter
     * @param xmpp
     * @param name
     * @return
     */
    Filter * getFilter(Xmpp* xmpp, const QString& name);
    static const QString MATRIX_USER;
    static const QString HOMESERVER;
    static const QString DEVICE_ID;
    static const QString DISPLAY_NAME;
    static const QString ACCESS_TOKEN;
    static const QString XMPP_CA;
    bool dbConnected;

signals:

public slots:
    void fillMatrixList(const QString& account,QList<Matrix *> &list);
    void fillFilterList(const QString& account,QList<Filter *> &list);

private:
    QSettings qsettings;
    QString getXmppAccountValue(const QString& account, const QString&key);
    bool setXmppAccountValue(const QString& account,const QString&key,const QString&value);
    bool setMatrixAccountValue(const QString& xmpp_account,const QString& matrix_account,const QString&key,const QString&value);
    void beginFilterGroup(const QString& account);
    bool addDbSchema();
    /**
     * @brief getXmppId fetches (or creates) xmpp account from database with the given account name
     * @param account xmpp.account_name
     * @return xmpp.id from database
     */
    int getXmppId(const QString& account, bool createIfNeeded=true);
    int getMatrixIdWithDisplayName(int xmpp_id,const QString& displayName,bool createIfNeeded=true);
    int getMatrixIdWithUserId(int xmpp_id,const QString& userId);
    int getMatrixId(int xmpp_id,const QString& key,const QString& value,bool createIfNeeded=true);
};

#endif // SETTINGS_H
