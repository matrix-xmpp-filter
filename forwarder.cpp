#include "forwarder.h"
#include "logging.h"
#include "settings.h"
#include<QDebug>
#include<QCoreApplication>
#include<QTimer>


Forwarder::Forwarder()
{
    xmppRetries=0;
}

void Forwarder::start(bool xmppServer)
{
    this->xmppServer=xmppServer;
    if(xmppServer) {
        xs=new XmppServer;
        xs->startServer();
        QObject::connect(xs,&XmppServer::fromXmpp,this,&Forwarder::messageFromXmppServerMode);
        QObject::connect(xs,&XmppServer::newConnection,this,&Forwarder::newXmppConnection);
    }
    startAccounts();
}

void Forwarder::startAccounts() {
    QStringList accounts=Settings::getInstance().xmppAccountNames();
    if(accounts.isEmpty()) {
        qCDebug(CONFIG) << "WARNING: no xmpp accounts configured";
    }
    for(const QString& account:accounts) {
        if(!xmppsByAccountName.contains(account))
            startAccount(account);
    }
}

/**
 * @brief Forwarder::newXmppConnection runs startAccounts if account of given jid is not running.
 * This is needed when new accounts are added to database without restarting matrix-xmpp-filter
 * @param jid
 */
void Forwarder::newXmppConnection(const QString& jid) {
    if(getXmpp(jid)==nullptr)
        startAccounts();
}

void Forwarder::startAccount(const QString& account) {
        Xmpp * x=new Xmpp;
        x->forwarder=this;
        x->account=account;
        xmppsByAccountName[account]=x;
        xmppsByJid[Settings::getInstance().getJid(account)]=x;
        if(xmppServer) {
            // in server mode messages are sent to jid given with -j command line option
            x->setXmppRecipient(Settings::getInstance().getJid(account));
            x->connectMatrix();
        }else{
            x->clientMode();
            // in client mode messages are sent to jid given with -r command line option
            x->setXmppRecipient(Settings::getInstance().getRecipient(account));
            x->connectToServer(Settings::getInstance().getJid(account),Settings::getInstance().getJpass(account));
        }
}

void Forwarder::stopAccount(const QString& account) {
    Xmpp *x = xmppsByAccountName[account];
    if(x!=nullptr) {
        x->disconnectMatrix();
        x->disconnectXmpp();
        xmppsByJid.remove(Settings::getInstance().getJid(x->account));
        xmppsByAccountName.remove(x->account);
    }
}


void Forwarder::messageFromXmppServerMode(const QString &from, const QString &to, const QString &msg)
{
    if(msg.isEmpty()) {
        qCDebug(XMPP) << "empty from xmpp";
        return;
    }
    qCDebug(XMPP) << "from xmpp:" << msg;
    Xmpp*x=getXmpp(from);
    if(x==nullptr) {
        qCDebug(XMPP) << "No Xmpp object found with jid " << from;
        return;
    }
    QString reply;
    if(msg.startsWith(QLatin1String(" /"))) {
        reply=x->command(msg.mid(2));
    }else if(msg.startsWith(QLatin1String("/"))) {
        reply=x->command(msg.mid(1));
    }else{
        Room * r=x->getRoomWithJid(to);
        if(r!=nullptr) {
            QString txnId=r->postPlainText(msg);
            matrixOutgoingTransactions[txnId]=QPair<QString,QString>(from,to);
            return;
        }else{
            reply=QStringLiteral("Room not found");
        }
    }
    xs->toXmpp(to,from,reply);
}

Xmpp *Forwarder::getXmpp(const QString &jid)
{
    Xmpp*x=xmppsByJid[jid];
    if(x!=nullptr) {
        return x;
    }
    auto index=jid.lastIndexOf('/');
    if(index==-1)
        return nullptr;
    return xmppsByJid[jid.left(index)];
}


void Forwarder::matrixMessageSentServerMode(QString txnId, QString eventId)
{
    qCDebug(MATRIX) << "SENT" << eventId;
    QPair<QString,QString> jids=matrixOutgoingTransactions[txnId];
    xs->toXmpp(jids.second,jids.first,QStringLiteral("SENT"));
    matrixOutgoingTransactions.remove(txnId);
}



const Xmpp *Forwarder::getXmpp(Matrix * const m)
{
    return xmppsByMatrix[m];
}


