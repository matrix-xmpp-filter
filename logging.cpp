#include "logging.h"

Q_LOGGING_CATEGORY(COMMAND,"mxf.cmd",QtInfoMsg)
Q_LOGGING_CATEGORY(CONFIG,"mxf.conf",QtInfoMsg)
Q_LOGGING_CATEGORY(DATABASE,"mxf.db",QtInfoMsg)
Q_LOGGING_CATEGORY(MATRIX,"mxf.matrix",QtInfoMsg)
Q_LOGGING_CATEGORY(MISC,"mxf.misc",QtInfoMsg)
Q_LOGGING_CATEGORY(XMPP,"mxf.xmpp",QtInfoMsg)
Q_LOGGING_CATEGORY(FILTER,"mxf.filter",QtInfoMsg)
