#include "logging.h"
#include "xmpppasswordchecker.h"
#include "settings.h"

XmppPasswordChecker::XmppPasswordChecker(QObject *parent) : QObject(parent)
{

}

/// Retrieves the password for the given username.
QXmppPasswordReply::Error XmppPasswordChecker::getPassword(const QXmppPasswordRequest &request, QString &password)
{
    QString pass=Settings::getInstance().getJpassWithUsername(request.username());
    if(pass.isNull()) {
        qCDebug(XMPP) << "password checker: no such user:" << request.username();
        return QXmppPasswordReply::AuthorizationError;
    }
    password = pass;
    qCDebug(XMPP) << "password="<<pass;
    return QXmppPasswordReply::NoError;
}

/// Returns true as we implemented getPassword().
bool XmppPasswordChecker::hasGetPassword() const
{
    return true;
}
