#!/bin/sh

version=1.3.1
set -e
if test -d /usr/lib/ccache ; then
	export PATH="/usr/lib/ccache:$PATH"
fi
project=qxmpp
dir="$(dirname "$0")"
cd "$dir"/qxmpp
git fetch
git reset --hard
git clean -f
git checkout v${version}
cd ..
rm -rf build_${project} install_${project}
mkdir -p build_${project} install_${project}/root
cd build_${project}
cmake ../${project}
make
tgz=${project}-${version}.tgz
fakeroot bash <<EOF
make DESTDIR=../install_${project}/root install
cd ../install_${project}/root
tar cf - .|gzip -1 >../$tgz
cd ..
alien $tgz
EOF
