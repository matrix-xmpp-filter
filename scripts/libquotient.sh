#!/bin/sh

set -e
dir="$(dirname "$0")"
cd "$dir"/libQuotient
git pull
cd ..
rm -rf build_libquotient install_libquotient
mkdir -p build_libquotient install_libquotient/root
cd build_libquotient
if test -d /usr/lib/ccache ; then
	export PATH=/usr/lib/ccache:$PATH
fi
cmake ../libQuotient
make -j7
tgz=libquotient-$(date -I|tr -d -).tgz
fakeroot bash <<EOF
make DESTDIR=../install_libquotient/root install
cd ../install_libquotient/root
tar cf - .|gzip -1 >../$tgz
cd ..
alien $tgz
EOF
