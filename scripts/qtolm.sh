#!/bin/sh

set -e
dir="$(dirname "$0")"
cd "$dir"/libqtolm
git pull
cd ..
rm -rf build_qtolm install_qtolm
mkdir -p build_qtolm install_qtolm/root
cd build_qtolm
cmake ../libqtolm
make -j7
tgz=qtolm-$(date -I|tr -d -).tgz
fakeroot bash <<EOF
make DESTDIR=../install_qtolm/root install
cd ../install_qtolm/root
tar cf - .|gzip -1 >../$tgz
cd ..
alien $tgz
EOF
