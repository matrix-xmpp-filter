#!/bin/sh

set -e
dir="$(dirname "$0")"
cd "$dir"/olm
git pull
cd ..
rm -rf build_olm install_olm
mkdir -p build_olm install_olm/root
cd build_olm
cmake ../olm
make -j7
tgz=olm-$(date -I|tr -d -).tgz
fakeroot bash <<EOF
make DESTDIR=../install_olm/root install
cd ../install_olm/root
tar cf - .|gzip -1 >../$tgz
cd ..
alien $tgz
EOF
