#include "matrix.h"
#include "logging.h"

Matrix::Matrix(const QString &name, const int syncLoopTimeout) {
    c.setLazyLoading(true);

    this->syncLoopTimeout=syncLoopTimeout;
    displayName=name;
    roomsConnected=false;
    connect(&c,&Connection::connected,this,&Matrix::connectedToMatrix);
    connect(&c,&Connection::syncDone, this,&Matrix::syncDone);
    connect(this,&Matrix::syncMatrix,&c,&Connection::sync);
    connect(&c,&Connection::newRoom,this,[this] (Room* room) {
        qCDebug(MATRIX) << displayName << "new room:" << room->displayName();
    });
    connect(&c,&Connection::invitedRoom,this,[this] (Room* room,Room * prev) {
        qCDebug(MATRIX) << displayName << "invitedRoom";
        printRoomInfo("room",room);
        printRoomInfo("prev",prev);
    });
    connect(&c,&Connection::joinedRoom,this,[this] (Room* room,Room * prev) {
        qCDebug(MATRIX) << displayName << "joinedRoom";
        printRoomInfo("room",room);
        printRoomInfo("prev",prev);
    });
    connect(&c,&Connection::leftRoom,this,[this] (Room* room,Room * prev) {
        qCDebug(MATRIX) << displayName << "leftRoom";
        printRoomInfo("room",room);
        printRoomInfo("prev",prev);
    });
    connect(&c,&Connection::aboutToDeleteRoom,this,[this] (Room* room) {
        qCDebug(MATRIX) << displayName << "aboutToDeleteRoom";
        printRoomInfo("room",room);
    });
    connect(&c,&Connection::createdRoom,this,[this] (Room* room) {
        qCDebug(MATRIX) << displayName << "createdRoom";
        printRoomInfo("room",room);
    });
    connect(&c,&Connection::loadedRoomState,this,[this] (Room* room) {
        qCDebug(MATRIX) << displayName << "loadedRoomState";
        printRoomInfo("room",room);
    });
    connect(&c,&Connection::accountDataChanged,this,[this] (QString type) {
        qCDebug(MATRIX) << displayName << "accountDataChanged" << type;
    });
}

Room *Matrix::getRoom(const QString &name)
{
    QVector<Room*> rooms=c.rooms(Quotient::JoinState::Join);
    for(Room * room:rooms) {
        if(room==nullptr) {
            qCDebug(MATRIX) << displayName << "null room";
            continue;
        }
        //qCDebug() << "room:" << ite.key().first << "-" << ite.key().second << "-" << ite.value()->name() << ite.value()->displayName();
        qCDebug(MATRIX) << displayName << "room displayName:" << room->displayName();
        if(room->displayName()==name) {
            return room;
        }
    }
    return nullptr;
}

QStringList Matrix::roomNames()
{
    QStringList ret;
    QVector<Room*> rooms=c.rooms(Quotient::JoinState::Join);
    for(Room * room:rooms) {
        if(room==nullptr) {
            continue;
        }
        ret.append(room->displayName());
    }
    return ret;
}

void Matrix::connectedToMatrix() {
    qCDebug(MATRIX) << displayName << "matrix connected, syncing";
    emit syncMatrix();
}

void Matrix::roomBaseStateLoaded() {
    qCDebug(MATRIX) << displayName << "roomBaseStateLoaded";
}

void Matrix::syncDone() {
    qCDebug(MATRIX) << displayName << "initial matrix sync done";

    QTimer::singleShot(1000, this, &Matrix::connectRooms);
    ++sync_count;

    disconnect(&c,&Connection::syncDone, this, &Matrix::syncDone);
    connect(&c,&Connection::syncDone, this, [this] () {
        //qCDebug() << displayName << "syncDone: " << sync_count << "syncs in" << (time(nullptr)-start_time) << "seconds";
        emit syncDoneDone();
    });

    start_time=time(nullptr);
    sync_count=0;
    if(syncLoopTimeout >= 0) {
        qCDebug(MATRIX) << displayName << "starting syncloop with timeout " << syncLoopTimeout;
        c.syncLoop(syncLoopTimeout);
    }else{
        qCDebug(MATRIX) << displayName << "NOT starting libQuotient syncLoop";
        //startSyncTimer();
    }

    qCDebug(MATRIX) << displayName << "initialSyncDone done";
    emit syncDoneDone();
}

void Matrix::connectRooms() {
    QVector<Room*> rooms=c.rooms(Quotient::JoinState::Join);
    int n=rooms.size();
    qCDebug(MATRIX) << displayName << "rooms: " << n;
    for(Room * room:rooms) {
        if(room==nullptr) {
            qCDebug(MATRIX) << displayName << "null room";
            continue;
        }
        //qCDebug() << "room:" << ite.key().first << "-" << ite.key().second << "-" << ite.value()->name() << ite.value()->displayName();
        qCDebug(MATRIX) << displayName << "room displayName:" << room->displayName();
        //connect(room,&Room::baseStateLoaded,this, &Matrix::roomBaseStateLoaded);
        connect(room,&Room::baseStateLoaded,this, [this,room]() {
            this->printRoomInfo("baseStateLoaded",room);
        });
        connect(room,&Room::eventsHistoryJobChanged,this, [this,room] {
            printRoomInfo("eventsHistoryJobChanged",room);
        });
        connect(room,&Room::addedMessages,this, [this,room] {
            printRoomInfo("addedMessages",room);
        });
        connect(room,&Room::pendingEventAdded,this, [this,room] {
            printRoomInfo("pendingEventAdded",room);
        });
        connect(room,&Room::pendingEventMerged,this, [this,room] {
            printRoomInfo("pendingEventMerged",room);
        });
        connect(room,&Room::pendingEventDiscarded,this, [this,room] {
            printRoomInfo("pendingEventDiscarded",room);
        });
        connect(room,&Room::topicChanged,this, [this,room] {
            printRoomInfo("topicChanged",room);
        });
        connect(room,&Room::avatarChanged,this, [this,room] {
            printRoomInfo("avatarChanged",room);
        });
        connect(room,&Room::memberListChanged,this, [this,room] {
            printRoomInfo("memberListChanged",room);
        });
        connect(room,&Room::allMembersLoaded,this, [this,room] {
            printRoomInfo("allMembersLoaded",room);
        });
        connect(room,&Room::typingChanged,this, [this,room] {
            printRoomInfo("typingChanged",room);
        });
        connect(room,&Room::highlightCountChanged,this, [this,room] {
            printRoomInfo("highlightCountChanged",room);
        });
        connect(room,&Room::notificationCountChanged,this, [this,room] {
            printRoomInfo("notificationCountChanged",room);
        });
        connect(room,&Room::firstDisplayedEventChanged,this, [this,room] {
            printRoomInfo("firstDisplayedEventChanged",room);
        });
        connect(room,&Room::lastDisplayedEventChanged,this, [this,room] {
            printRoomInfo("lastDisplayedEventChanged",room);
        });
        connect(room,&Room::addedMessages,this,[this,room] (int fromIndex,int toIndex) {
            printRoomInfo("addedMessages", room);
            qCDebug(MATRIX) << displayName << fromIndex << toIndex;
            for(unsigned long i=static_cast<unsigned long>(fromIndex) ; i <= static_cast<unsigned long>(toIndex) ; i++) {
                processMessage(room,i);
            }
        });
        connect(room,&Room::changed,this,[this,room] (QMatrixClient::Room::Changes changes) {
            printRoomInfo("changed",room);
            qCDebug(MATRIX) << displayName << changes;
        });
        emit roomConnected(room);
    }
    roomsConnected=true;
}

void Matrix::processMessage(Room *room, unsigned long i) {
    const RoomEvent* re=room->messageEvents().at(i).get();
    qCDebug(MATRIX) << displayName << "type:"<< re->matrixType();
    qCDebug(MATRIX) << displayName << "json:"<< re->fullJson();
    if(QStringLiteral("m.room.message")==re->matrixType()) {
        auto content=re->fullJson().value(QLatin1String("content")).toObject();
        QJsonValue msgtype=content.value(QLatin1String("msgtype"));
        QJsonValue body=content.value(QLatin1String("body"));
        QString sender=room->roomMembername(re->senderId());
        if(msgtype.isString()) {
            auto mt=msgtype.toString();
            if(mt==QLatin1String("m.image") || mt==QLatin1String("m.video") || mt==QLatin1String("m.audio") || mt==QLatin1String("m.file")) {
                auto urlJson=content.value(QLatin1String("url"));
                if(urlJson.isString()) {
                    auto u=DownloadFileJob::makeRequestUrl(c.homeserver(), urlJson.toString());
                    emit forward(this, room, sender, (body.isString() ? (body.toString()+QStringLiteral(" ")+u.toString()) : u.toString()));
                    return;
                }
            }
        }
        if(body.isString()) {
            emit forward(this, room, sender, body.toString());
        }else{
            qCDebug(MATRIX) << displayName << "Not emitting";
        }

    }
}

void Matrix::syncNow()
{
    emit syncMatrix(-syncLoopTimeout);
}

QByteArray Matrix::accessToken() const
{
    return c.accessToken();
}

QString Matrix::deviceId() const
{
    return c.deviceId();
}

QString Matrix::name() const
{
    return displayName;
}

void Matrix::disconnectMatrix()
{
    c.stopSync();
}

void Matrix::createAccessToken(const QString &userId, const QString &password, const QString &homeserver, const QVariant &deviceId)
{
    qCDebug(MATRIX) << displayName << "remove connection";
    disconnect(&c,&Connection::connected,this,&Matrix::connectedToMatrix);
    c.setHomeserver(QUrl(homeserver));
    qCDebug(MATRIX) << displayName << "add connection";
    connect(&c,&Connection::connected,this,[this] () {
        qCDebug(MATRIX) << displayName << "connected. emitting accessTokenCreated";
        emit accessTokenCreated();
    });
    connect(&c,&Connection::loginError, this, [this] (const QString & message, const QString & details) {
        qCDebug(MATRIX) << displayName << "Login ERROR: " << message << " " << details;
        emit accessTokenCreateFailed();
    });
    qCDebug(MATRIX) << displayName << "connecting";
    c.connectToServer(userId,password,deviceId.toString());
}

void Matrix::printRoomInfo(const char * text, Room * r)
{
    qCDebug(MATRIX) << displayName << text << (r==nullptr?QStringLiteral("(null)"):r->displayName());
}

void Matrix::printRoomInfo(Room * r)
{
    printRoomInfo("",r);
}


//token: https://matrix.org/docs/guides/client-server.html -> Login

void Matrix::connectToMatrix(const QString &homeserver, const QString &userId, const QString& accessToken, const QString& deviceId) {
    c.setHomeserver(QUrl(homeserver));
    c.connectWithToken(userId, accessToken, deviceId);
}
